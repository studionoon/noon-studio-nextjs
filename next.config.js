module.exports = {
  images: {
    domains: [
      "tomorrow.testing-noon.studio",
      "secure.gravatar.com",
      "noon.studio",
      "api.noon.studio",
      "localhost:8001",
    ],
  },
};
