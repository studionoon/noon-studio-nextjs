(function() {


	var pushingNavTrigger = document.getElementById('nav_button');
	// console.log(pushingNavTrigger, pushingNavTrigger.length);


	if(pushingNavTrigger) {
		var mainContent = document.getElementById('nav_main__content'),
			navAnimating = false;
			// console.log(mainContent);

		pushingNavTrigger.addEventListener('click', function(event) {
			event.preventDefault();
			// if(navAnimating) return; // already animating -> do not toggle
			// navAnimating = true;

            document.body.classList.toggle('nav_is_open');
		});

		mainContent.addEventListener('transitionend', function(){
			navAnimating = false; // wait for the ened of animation to reset the variable
		});
	}
}());