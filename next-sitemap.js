module.exports = {
  siteUrl: process.env.SITE_URL || "https://noon.studio",
  generateRobotsTxt: true,
};
