import { NextResponse } from 'next/server';

const Middleware = (req) => {
  if (req.nextUrl.pathname === req.nextUrl.pathname.toLowerCase())
    return NextResponse.next();

  return NextResponse.redirect(new URL(req.nextUrl.origin + req.nextUrl.pathname.toLowerCase()));
};

export default Middleware;

// Might affect performance as a heads up from stack overflow
// https://stackoverflow.com/questions/63695742/redirect-in-next-js-from-uppercase-to-lowercase-url
// https://nextjs.org/docs/advanced-features/middleware

// Middleware is javascript that runs between every request so hence why it can be used here to change the url to lowercase