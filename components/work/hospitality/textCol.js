import styles from './mediaText.module.scss'

export default function WorkTextCol() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['colText']}` }>

                    <div className="row">

                        <h3>The solution</h3>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>

                            <h4>Brand Identity &#38; Guidelines</h4>
                          
                            <p>We introduced a typeface and logotype which unified the brand and complimented the parent brand identity. The versatile logotype alongside a luxury colour palette capturing the brand essence in all advertising and marketing.</p>

                        </div>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>

                            <h4>Bringing online in-line</h4>
                          
                            <p>Using these brand guidelines we designed and built a WordPress website which expressed the Hospitality vision online. The sub-brands identity made it possible to create a cohesive yet clear discrepancy between different areas of the site.</p>
                            <p>With a varied audience it was important to design a navigation which made it easy for users to reach the areas of the site they wanted to. We utilised user journeys to understand different markets, resulting in the design of a concise and structured navigation.</p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
