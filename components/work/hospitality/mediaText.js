import styles from './mediaText.module.scss'
import Image from 'next/image'

export default function WorkMediaText() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `col-full ${styles['mediaCont']}` }>

                    <div className="row">

                        <div className={ `col-12 col-xl-6 ${styles['mediaCont_text']}` }>

                            <div className={ `${styles['textbg']}` }>

                                <div className={ `${styles['textwrapper']}` }>

                                    <h3>The Challenge</h3>

                                    <p>Hospitality services approached us looking to bring their new name and identity in line with their company values. The brand is split into four sub-brands each focussing on different offering. The challenge was knitting this into a cohesive brand whilst appealing to the different target audiences.</p>
                                    <p>A further consideration was how it could work across both digital and printed mediums – specifically focussing on the website as this is a key avenue for information and marketing.</p>

                                </div>
                            
                            </div>

                        </div>

                        <div className={ `col-12 col-xl-6 ${styles['mediaCont_image']}` }>

                            <div style={{width: '100%', height: '100%', position: 'relative'}}>

                                <Image
                                    layout="fill"
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-143.png"
                                    // height={50}
                                    // width={50}
                                    objectFit='cover'
                                />

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
        
    )
    
}
