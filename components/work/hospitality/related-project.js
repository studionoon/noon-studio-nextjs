import styles from './related-project.module.scss'
import Image from 'next/image'

export default function RelatedProject() {

    return (

    <>

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container container-small ${styles['col-full']}` }>

                    <div className="row">

                        <div className={ `col-12 col-md-5 ${styles['text-side']}` }>

                            <div className={` ${styles['menu-box']}`}>

                                <a className='' href="/work/growing-for-a-digital-age"><h2 className="h6">Next Project</h2></a>

                                <a className='' href="/work/growing-for-a-digital-age"><h3 className="h4">The Museum of English Rural Life</h3></a>

                                <a className='text-link' href="/work/growing-for-a-digital-age"><p className="mb-0">Website design <span className={'dash_colour'}></span> development&nbsp;&nbsp;<svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="arrow-right" className="svg-inline--fa fa-arrow-right fa-w-14 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M216.464 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L387.887 239H12c-6.627 0-12 5.373-12 12v10c0 6.627 5.373 12 12 12h375.887L209.393 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L233.434 36.465c-4.686-4.687-12.284-4.687-16.97 0z"></path></svg></p></a>

                            </div>

                        </div>

                        <div className={ `col-12 col-md-7 ${styles['image-side']}` }>

                            <a className='' href="/work/growing-for-a-digital-age">

                                <Image
                                    layout="fill"
                                    src="https://api.noon.studio/wp-content/uploads/2022/01/Mask-Group-153.png"
                                />

                            </a>

                        </div>

                    </div>
                
                </div>

            </div>

        </div>

    </>

    )
    
}
