import styles from './projectGallery.module.scss'

export default function WorkProjectGallery() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['mediaCont']}` }>

                    <div className={ `row ${styles['image_row']}` }>

                        <div className={ `col-12 ${styles['mediaCont_image']}` }>

                            <img
                                src="https://api.noon.studio/wp-content/uploads/2022/02/iphone-wireframes.png"
                                className='img-fluid'
                            />

                        </div>

                    </div>
                    <div className={ `row ${styles['image_row']}` }>

                        <div className={ `col-12 col-md-6 ${styles['mediaCont_image']} ${styles['mediaCont_imageLeft']}` }>

                            <img
                                src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-3.png"
                                className={`img-fluid ${styles['leftcolimage']}` }
                            />

                        </div>
                        <div className={ `col-12 col-md-6 ${styles['mediaCont_image']}` }>

                            <img
                                src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-3.png"
                                className='img-fluid'
                            />
                            <img
                                src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-3.png"
                                className='img-fluid'
                            />

                        </div>

                    </div>
                    <div className={ `row ${styles['image_row']}` }>

                        <div className={ `col-12 ${styles['mediaCont_image']}` }>

                            <img
                                src="https://api.noon.studio/wp-content/uploads/2022/02/virtual-open-day-desktop.png"
                                className='img-fluid'
                            />

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
