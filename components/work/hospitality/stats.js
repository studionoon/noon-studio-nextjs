import styles from './stats.module.scss'
import CountUp from 'react-countup';
import { InView } from 'react-intersection-observer'
import {  useState } from 'react';

export default function WorkStats() {

    const [inView, setInView] = useState(false);

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['WorkStats']}` }>

                    <div className="row">

                        <h3>The results</h3>

                        <div className={ `col-12 col-lg-5 ${styles['WorkStats_text']}` }>
                          
                            <p>Following the first Virtual Open Day, a survey found that 98% of prospective students rated their experience as good or excellent.</p>
                            <p>We’ve continued working with the University’s Global Recruitment team to develop further scalability and flexibility to the platform.</p>

                        </div>

                        <div className={ `col-12 col-lg-7 ${styles['WorkStats_text']}` }>

                            <div className={ `container ${styles['WorkStats']}` }>

                                <div className="row">

                                    <div className={ `col-12 ${styles['WorkStats_stats_group']}` }>

                                        <p className={ `${styles['stats_stats']}` }>

                                            <InView triggerOnce={true} as="false" onChange={setInView}>

                                                { inView && 
                                                
                                                    <CountUp
                                                    start={0}
                                                    end={999}
                                                    duration={3}
                                                    decimals={0}
                                                    decimal="."
                                                    suffix="%"
                                                    // onEnd={() => console.log('Ended! 👏')}
                                                    // onStart={() => console.log('Started! 💨')}
                                                    useEasing={true}
                                                    ></CountUp>

                                                }

                                            </InView> 

                                        </p>

                                        <p className={ `${styles['stats_subtitle']}` }>increase in yumminess compared to dumb cuisine</p>
                                
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
