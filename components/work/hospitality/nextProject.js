import styles from './nextProject.module.scss'
import Image from 'next/image'

export default function WorkMediaText() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `col-full ${styles['mediaCont']}` }>

                    <div className="row">

                        <div className={ `col-12 col-md-5 ${styles['mediaCont_text']}` }>

                            <div className={ `${styles['textbg']}` }>

                                <h3>Next Project</h3>

                                <a href="/"><h4>University of Reading Virtual Open Day</h4></a>

                                <p>Website design <span className={'dash_colour'}></span> development</p>
                            
                            </div>

                        </div>

                        <div className={ `col-12 col-md-7 ${styles['mediaCont_image']}` }>

                            <div style={{width: '100%', height: '100%', position: 'relative'}}>
                                
                                <a href="/">

                                    <Image
                                        layout="fill"
                                        src="https://api.noon.studio/wp-content/uploads/2022/01/Mask-Group-153.png"
                                        objectFit='cover'
                                    />

                                </a>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        

    )
    
}
