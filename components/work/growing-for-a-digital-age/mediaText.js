import styles from './mediaText.module.scss'
import Image from 'next/image'

export default function WorkMediaText() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `col-full ${styles['mediaCont']}` }>

                    <div className="row">

                        <div className={ `col-12 col-md-6 ${styles['mediaCont_text']}` }>

                            <div className={ `${styles['textbg']}` }>

                                <div className={ `${styles['textwrapper']}` }>

                                    <h3 className='h6 has-pseudo-line'>The Challenge</h3>

                                    <p>When we began working with The MERL in 2018 they didn’t have a huge digital presence with a lot of their advertising relying on physical promotion and word of mouth. </p>
                                    <p>The challenge was to realign The MERL’s brand strategy for the digital audience.</p>

                                </div>
                            
                            </div>

                        </div>

                        <div className={ `col-12 col-md-6 ${styles['mediaCont_image']}` }>

                            <div style={{width: '100%', height: '100%', position: 'relative'}}>

                                <Image
                                    layout="fill"
                                    src="https://api.noon.studio/wp-content/uploads/2022/01/Mask-Group-153.png"
                                    // height={50}
                                    // width={50}
                                    objectFit='cover'
                                />

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
