import styles from './quote.module.scss'


export default function WorkQuote() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['WorkQuote']}` }>

                    <div className="row justify-content-center">

                        <div className={ `col-auto ${styles['WorkQuote_text']}` }>
                          
                            <p className={ `${styles['stats_quotemark']}` }>"</p>
                            <p className={ `${styles['stats_quote']}` }>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>
                            <p className={ `${styles['stats_quotemark_2']}` }>"</p>
                            <p className={ `${styles['stats_name']}` }>Alison Hilton</p>
                            <p className={ `${styles['stats_title']}` }>Marketing Manager, The MERL</p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
