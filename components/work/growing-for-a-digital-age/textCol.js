import styles from './textCol.module.scss'

export default function WorkTextCol() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container` }>

                    <div className="row">

                        <h3 className='h6 has-pseudo-line'>The solution</h3>

                        <h4 className='h2'>A new digital identity</h4>

                        <div className={ `col-12 col-xl-5 col-md-6 ${styles['colText_text']}` }>
                        
                            <p className='intro'>By simplifying the messaging from factual and long-winded to witty and engaging, we were able to reposition their marketing to a wider digital audience. We focussed on what made them unique — English Rural History — and used imagery from their archives to illustrate the messaging.</p>

                        </div>

                        <div className={ `col-12 col-xl-5 col-md-6 ${styles['colText_text']}` }>

                            <p>We developed a strategy which would encourage viewers to grow from an audience into a community. The pillars of the strategy focussed on creating:</p>
                                <ol className={ `${styles['ol_list']}` }>
                                    <li>Compelling digital marketing</li>
                                    <li>Image-led email campaigns</li>
                                    <li>External advertising campaigns</li>
                                </ol>

                        </div>

                        <div className={`col-12 mt-5`}>

                            <div style={{width: '100%', height: 'auto', position: 'relative'}}>

                                <video className="_video_standard" loop muted autoPlay style={{width: '100%', height: '100%'}}>
                                    <source type="video/mp4" src="https://noon.studio/wp-content/uploads/2018/12/Comp-1_2.mp4" />
                                </video>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
