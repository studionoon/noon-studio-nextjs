import styles from './stats.module.scss'
import CountUp from 'react-countup';
import { InView } from 'react-intersection-observer'

import {  useState } from 'react';

export default function WorkStats() {

    const [inView, setInView] = useState(false);

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['WorkStats']}` }>

                    <div className="row">

                        <h2 className='h6 has-pseudo-line'>The results</h2>

                        <h3 className='h2'>A flourishing community</h3>

                        <div className={ `col-12 col-xl-4 ${styles['WorkStats_text']}` }>
                          
                            <p className='intro'>Alongside The MERL&apos;s marketing team, we&apos;ve helped grow a small digital user base into a community who visit the museum, café and shop&nbsp;regularly.</p>
                            <p className=''>The email campaigns have helped drive traffic to both the website and in turn, the museum.</p>
                            <p className=''>On average, the email open rate has been 23.4% above the sector average and the click-rate a huge 13.2% above the sector average.</p>

                        </div>

                        <div className={ `col-12 col-xl-8 ${styles['WorkStats_text']}` }>

                            <div className={ `${styles['WorkStats']}` }>

                                <div className="row justify-content-center">

                                    <div className={ `col-auto ${styles['WorkStats_stats_group']}` }>

                                        <p className={ `${styles['stats_title']}` }>Click rate</p>
                                        <p className={ `${styles['stats_stats']}` }>
                                            <InView triggerOnce={true} as="false" onChange={setInView}>

                                                { inView && 
                                                
                                                    <CountUp
                                                    start={0}
                                                    end={13.2}
                                                    duration={2}
                                                    decimals={1}
                                                    decimal="."
                                                    suffix="%"
                                                    useEasing={true}
                                                    ></CountUp>

                                                }

                                            </InView> 
                                        </p>
                                        <p className={ `${styles['stats_subtitle']}` }>Above the sector average</p>
                                
                                    </div>
                                    <div className={ `col-auto  ${styles['WorkStats_stats_group']}` }>

                                        <p className={ `${styles['stats_title']}` }>Open rate</p>
                                        <p className={ `${styles['stats_stats']}` }>
                                            <InView triggerOnce={true} as="false" onChange={setInView}>

                                                { inView && 
                                                
                                                    <CountUp
                                                    start={0}
                                                    end={23.4}
                                                    duration={2}
                                                    decimals={1}
                                                    decimal="."
                                                    suffix="%"
                                                    useEasing={true}
                                                    ></CountUp>

                                                }

                                            </InView> 
                                        </p>
                                        <p className={ `${styles['stats_subtitle']}` }>Above the sector average</p>
                                
                                    </div>
                                    <div className={ `col-auto  ${styles['WorkStats_stats_group']}` }>

                                        <p className={ `${styles['stats_title']}` }>Museum visitors</p>
                                        <p className={ `${styles['stats_stats']}` }>
                                            <InView triggerOnce={true} as="false" onChange={setInView}>

                                                { inView && 
                                                
                                                    <CountUp
                                                    start={0}
                                                    end={2000}
                                                    duration={2}
                                                    suffix="+"
                                                    useEasing={true}
                                                    ></CountUp>

                                                }

                                            </InView> 
                                        </p>
                                
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
