import styles from './projectGallery.module.scss'
import Image from 'next/image'



export default function WorkProjectGallery() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['mediaCont']}` }>

                    <div className={ `row ${styles['image_row']}` }>

                        <div className={ `col-12 col-md-6 ${styles['mediaCont_image']} ${styles['mediaCont_imageLeft']}` }>

                            {/* <img
                                src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-3.png"
                                className={`img-fluid ${styles['leftcolimage']}` }
                            /> */}

                            {/* <div className={ `${styles['mediaCont_image']}` }> */}

                                <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={930}
                                    className={`${styles['col-img']}`}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-3.png"
                                />

                            {/* </div> */}

                        </div>
                        <div className={ `col-12 col-md-6 ${styles['mediaCont_image']}` }>

                            <Image
                                height={1440}
                                width={1920}
                                src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-3.png"
                            />

                            <Image
                                height={1440}
                                width={1920}
                                src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-3.png"
                            />

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
