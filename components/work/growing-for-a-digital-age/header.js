import styles from './header.module.scss'
import Image from 'next/image'

const Icon = () => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="1920" height="1077.586" viewBox="0 0 1920 1077.586"><path id="Subtraction_1" data-name="Subtraction 1" d="M1920,2040H0V962.415c.033,15.809.453,31.795,1.249,47.512.822,16.23,2.07,32.641,3.707,48.778,1.626,16.018,3.68,32.191,6.105,48.069,2.408,15.772,5.249,31.686,8.442,47.3,6.329,30.944,14.288,61.914,23.656,92.049,9.263,29.8,20.125,59.488,32.282,88.246,12.045,28.493,25.646,56.741,40.425,83.961,14.674,27.026,30.852,53.671,48.086,79.194,17.151,25.4,35.745,50.277,55.265,73.942,19.472,23.607,40.318,46.556,61.96,68.209s44.578,42.51,68.173,61.992c23.655,19.532,48.52,38.136,73.9,55.293,25.509,17.242,52.139,33.429,79.152,48.111,27.206,14.787,55.44,28.395,83.918,40.446,28.743,12.164,58.418,23.031,88.2,32.3,30.119,9.373,61.073,17.336,92,23.668,15.6,3.195,31.511,6.037,47.275,8.447,15.872,2.427,32.036,4.482,48.044,6.108,16.128,1.639,32.531,2.886,48.753,3.709,16.352.829,32.973,1.25,49.4,1.25s33.049-.42,49.4-1.25c16.223-.823,32.626-2.071,48.753-3.709,16.008-1.627,32.172-3.682,48.044-6.108,15.764-2.41,31.669-5.252,47.275-8.447,30.93-6.333,61.884-14.3,92-23.668,29.783-9.268,59.458-20.135,88.2-32.3,28.477-12.051,56.711-25.659,83.918-40.446,27.012-14.681,53.643-30.868,79.152-48.111,25.386-17.159,50.251-35.763,73.9-55.293,23.594-19.481,46.531-40.339,68.173-61.992s42.488-44.6,61.96-68.209c19.519-23.663,38.112-48.541,55.265-73.942,17.234-25.523,33.412-52.167,48.086-79.194,14.779-27.22,28.38-55.469,40.425-83.961,12.157-28.758,23.019-58.448,32.282-88.246,9.369-30.138,17.328-61.108,23.656-92.049,3.194-15.619,6.035-31.533,8.442-47.3,2.425-15.878,4.479-32.051,6.1-48.069,1.637-16.132,2.885-32.543,3.707-48.778.8-15.715,1.216-31.7,1.247-47.511Z" transform="translate(0 -962.415)" fill="#02182b"/>
        </svg>
    );
}
   
export default function WorkHeader() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={ `${styles['title_wrapper']}` }>

                <div className="row">

                    <h1>Growing for a digital&nbsp;age</h1>

                </div>

                <div className={styles.image_bg}></div>

                <Image
                    layout="fill"
                    src="https://api.noon.studio/wp-content/uploads/2022/01/Mask-Group-153.png"
                />

                <Icon/>

            </div>
            
            <div className={styles['content']}>

                <div className={ `container ${styles['col-client']}` }>

                    <div className="row">

                        <div className={ `col-12 col-lg-6 ${styles['col-background']}` }>

                            <h3 className="has-pseudo-line">Background</h3>

                            <p>Set amid the commotion of Reading, The Museum of English Rural Life is set in stark contrast to its surroundings. The free-to-visit museum showcases artefacts and stories from agriculture to rural life, challenging perceptions of what rural life can be.</p>

                        </div>

                        <div className={ `col-12 col-md-6 col-lg-3 ${styles['col-client']}` }>

                            <h3 className="has-pseudo-line">Client</h3>

                            <p>The Museum of English Rural&nbsp;Life</p>

                        </div>

                        <div className={ `col-12 col-md-6 col-lg-3 ${styles['col-services']}` }>

                            <h3 className="has-pseudo-line">Services</h3>

                            <ul>
                                <li>Brand Identity</li>
                                <li>Website Development</li>
                                <li>Digital Design</li>
                            </ul>

                        </div>

                    </div>
                
                </div>

            </div>

        </div>

    )
    
}
