import styles from './related-project.module.scss'
import Image from 'next/image'
 
export default function RelatedProject() {

    return (

        <>

            <div className={ `${styles['wrapper']}` }>

                <div className={styles['content']}>

                    <div className={ `container container-small ${styles['col-full']}` }>

                        <div className="row">

                            <div className={ `col-12 col-md-5 ${styles['text-side']}` }>

                                <div className={` ${styles['menu-box']}`}>

                                    <a className='' href="/work/virtual-reading"><h2 className="h6 has-pseudo-line">Next Project</h2></a>

                                    <a className='' href="/work/virtual-reading"><h3 className="h4">University of Reading</h3></a>

                                    <a className='text-link' href="/work/virtual-reading"><p className="mb-0">Website design <span className={'dash_colour'}></span> Development</p></a>

                                </div>

                            </div>


                        <div className={ `col-12 col-md-7 ${styles['image-side']}` }>

                            <a className='' href="/work/virtual-reading">

                                <Image
                                    layout="fill"
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-3.png"
                                />

                            </a>

                        </div>

                    </div>
                    </div>

                </div>

            </div>

        </>

    )
    
}
