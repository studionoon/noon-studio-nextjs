import Image from 'next/image'
import styles from './image_lead_emails.module.scss'


export default function ImageLeadEmails() {

    return (

        <div className={ `wrapper ${styles.wrapper}` }>

            <div className={`container`}>

                <div className="row">

                    <h3 className='h2'>Image-led Email Campaigns</h3>

                    <div className={ `col-12` }>
                        
                        <p className='intro'>Using the brand identity, we crafted a template for engaging, image-led email newsletters to allow the marketing team to reach out and inform subscribers of upcoming events, lectures, and exhibitions. Research and testing allowed us to ensure users responded to image-led marketing.</p>

                        <div className={styles.spacer} />

                        <div style={{width: '100%', height: 'auto', position: 'relative'}}>
{/* 
                            <Image
                                layout="fill"
                                src="https://api.noon.studio/wp-content/uploads/2022/02/merl-email-video-still-desktop.png"
                                // height={50}
                                // width={50}
                                objectFit='cover'
                            /> */}

                            <video className="_video_standard" loop muted autoPlay style={{width: '100%', height: '100%'}}>
                                <source type="video/mp4" src="https://noon.studio/wp-content/uploads/2018/12/Comp-1_2.mp4" />
                            </video>

                        </div>

                    </div>
                    
                </div>

            </div>

        </div>

    )
    
}
