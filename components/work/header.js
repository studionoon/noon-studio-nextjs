import styles from "./header.module.scss";

export default function WorkHeader() {
  return (
    <>
      <div className={`${styles["wrapper"]}`}>
        <div className={styles["content"]}>
          <div style={{ height: "100px" }}></div>

          <div className={`container ${styles["col-client"]}`}>
            <div className="row">
              <div
                className={`col-12 col-md-10 col-lg-8 col-xl-6 ${styles["col-client"]}`}
              >
                <h2>We work with ambitious clients&nbsp;of all sizes</h2>

                <p className={`intro`}>
                  We can't wait to share our recent projects soon
                </p>
                {/* <p className={`intro`}>Take a look through our recent work to see how we&apos;ve helped shape ideas into successful solutions.</p> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
