import styles from './quote.module.scss'


export default function WorkQuote() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['WorkQuote']}` }>

                    <div className="row">

                        <div className={ `col-12 ${styles['WorkQuote_text']}` }>
                          
                            <p className={ `${styles['stats_quotemark']}` }>"</p>
                            <p className={ `${styles['stats_quote']}` }>“Studio Noon have provided us with a fantastic service, from both a technical and design perspective. They are solution driven, providing a range of options to help meet our needs…Our Virtual Open Day site was shortlisted for a Higher Education ‘Best Open Day’ award in June 2020, a real achievement given the timescales to create it and competition from other Universities with existing virtual sites pre-pandemic to build on.</p>
                            <p className={ `${styles['stats_quote']}` }>We have very much enjoyed working with Studio Noon in partnership to create the Virtual Reading site, and are proud of the result.“</p>
                            <p className={ `${styles['stats_quotemark_2']}` }>"</p>
                            <p className={ `${styles['stats_name']}` }>Rachel South</p>
                            <p className={ `${styles['stats_title']}` }>Head of Global Recruitment, The University of Reading</p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
