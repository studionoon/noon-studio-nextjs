import styles from './partners.module.scss'
import Image from 'next/image' 

export default function Partners() {

    return (

        <div className={ `${styles['wrapper']}` }>
            
            <div className={styles['content']}>

                <div className={ `container` }>
                
                    <div className={ `${styles['text-wrapper']}` }>

                        <h3 className="h6 has-pseudo-line">Our Partners</h3>
                        <h4 className="h3">Here are just some of the companies we&apos;re proud of partnering with:</h4>

                    </div>

                    <div className={ `row ${styles['icon-row']}` }>

                        <div className={ `col-6 col-sm-4 ${styles['col-lg-2point4']} ${styles['col-project']}` }>

                            <Image
                                layout="fill"
                                src="https://api.noon.studio/wp-content/uploads/2023/01/client__UoR.svg"
                            />

                        </div>

                        <div className={ `col-6 col-sm-4 ${styles['col-lg-2point4']} ${styles['col-project']}` }>

                            <Image
                                layout="fill"
                                src="https://api.noon.studio/wp-content/uploads/2023/01/client__UoW.svg"
                            />

                        </div>

                        <div className={ `col-6 col-sm-4 ${styles['col-lg-2point4']} ${styles['col-project']}` }>

                            <Image
                                layout="fill"
                                src="https://api.noon.studio/wp-content/uploads/2023/01/client__UoS.svg"

                            />

                        </div>

                        <div className={ `col-6 col-sm-6 ${styles['col-lg-2point4']} ${styles['col-project']}` }>

                            <Image
                                layout="fill"
                                src="https://api.noon.studio/wp-content/uploads/2023/01/client__RHU.svg"
                            />

                        </div>

                        <div className={ `col-6 col-sm-6 ${styles['col-lg-2point4']} ${styles['col-project']}` }>

                            <Image
                                layout="fill"
                                src="https://api.noon.studio/wp-content/uploads/2023/01/client__MERL.svg"
                            />

                        </div>

                        <div className={ `col-6 col-sm-6 ${styles['col-lg-2point4']} ${styles['col-project']}` }>

                            <Image
                                layout="fill"
                                src="https://api.noon.studio/wp-content/uploads/2023/01/client__RM.svg"
                                />

                        </div>
                        
                        <div className={ `col-6 col-sm-6 ${styles['col-lg-2point4']} ${styles['col-project']}` }>

                            <Image
                                layout="fill"
                                src="https://api.noon.studio/wp-content/uploads/2023/01/client__SM.svg"
                                />

                        </div>

                    </div>
                
                </div>

            </div>

        </div>

    )
    
}
