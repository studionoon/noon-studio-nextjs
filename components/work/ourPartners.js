import styles from './ourPartners.module.scss'
import Image from 'next/image'

   

export default function ourPartners() {

    return (

        <div className={ `${styles['wrapper']}` }>
            
            <div className={styles['content']}>

                <div className={ `container ${styles['col-client']}` }>

                    <div className="row">

                    <h3>Our Partners</h3>

                    <p>Here are just some of the companies we&apos;re proud of partnering with:</p>

                        <div className={ `col-12 col-md-6 col-lg-4 col-xl ${styles['col-partners']}` }>

                            <img src="https://api.noon.studio/wp-content/uploads/2021/11/Group-152-300x69.png" />

                        </div>
                        <div className={ `col-12 col-md-6 col-lg-4 col-xl ${styles['col-partners']}` }>

                            <img src="https://api.noon.studio/wp-content/uploads/2021/11/Group-158.png" />

                        </div>
                        <div className={ `col-12 col-md-6 col-lg-4 col-xl ${styles['col-partners']}` }>

                            <img src="https://api.noon.studio/wp-content/uploads/2021/11/Group-162.png" />

                        </div>
                        <div className={ `col-12 col-md-6 col-lg-4 col-xl ${styles['col-partners']}` }>

                            <img src="https://api.noon.studio/wp-content/uploads/2021/11/Group-159.png" />

                        </div>
                        <div className={ `col-12 col-md-6 col-lg-4 col-xl ${styles['col-partners']}` }>

                            <img src="https://api.noon.studio/wp-content/uploads/2021/11/Group-155.png" />

                        </div>
                        <div className={ `col-12 col-md-6 col-lg-4 col-xl ${styles['col-partners']}` }>

                            <img src="https://api.noon.studio/wp-content/uploads/2021/11/Group-159.png" />

                        </div>
                        <div className={ `col-12 col-md-6 col-lg-4 col-xl ${styles['col-partners']}` }>

                            <img src="https://api.noon.studio/wp-content/uploads/2021/11/Group-155.png" />

                        </div>

                    </div>
                
                </div>

            </div>

        </div>

    )
    
}
