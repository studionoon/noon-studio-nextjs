export default function Mockup() {

    return(

        <section className="py-5">

            <div className="container">

                <div className='row justify-content-center'>

                    <div className="col-4 text-center">

                        <h3>Impact</h3>

                        <p>Utilising research, personas and user journeys we outlined that visitors had found it difficult to navigate through the existing site and a more intuitive user journey was required.</p>
                        <p>This fed into wireframes and helped us create an experience which led to an easier route for donations and get involved volunteering opportunities.</p>
                        <p><a href="#" className="btn btn-primary br-0">Experience it Here</a></p>

                    </div>

                </div>

            </div>

        </section>


    )

}