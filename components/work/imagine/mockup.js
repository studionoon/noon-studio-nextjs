import Image from 'next/image'
import styles from './mockup.module.scss'

import mockup from './mockup.jpeg'
import mockupHeader from './mockup-header.png'

import React, { useRef, useState, useEffect } from 'react';

export default function Mockup() {

    const ratio = 0.5625;
    const ref = useRef(null);
    const [height, setHeight] = useState(0)

    useEffect(() => {

        const handleResize = () => {

            let newHeight = ( ref.current )  ? ref.current.offsetWidth * ratio : 0
            setHeight( newHeight )
        }


        window.addEventListener("load", handleResize)
        window.addEventListener("resize", handleResize)

        return () => {
            window.removeEventListener("resize", handleResize)
        }

    }, [ref.current]);

    return (

        <section className={ `${styles['section']}` }>

            <div className='container'>

                <div className="row justify-content-center">
                    
                    <div className="col col-image-sm opacity-overlay">
                        
                                            
                        <div ref={ref} className={ `${styles['website_container']}` }>

                            <Image
                                src={mockupHeader}
                            />
                            
                            <div className={ `${styles['website-height']}` } style={ { height }}>
                                
                                                            
                                <div className={ `${styles['image-wrap']}` }>   

                                    <Image
                                        src={ mockup }
                                    />
                                    
                                    <div className={ `${styles['overlay']}` }>
                                                                        
                                        <div id="mouse_body" className={ `${styles['mouse_body']}` }>
                                            <div id="mouse_wheel" className={ `${styles['mouse_wheel']}` }></div>
                                        </div>
                                            
                                    </div>

                                </div>
                                
                            </div>                                 

                        </div>

                    </div>

                </div>
                
            </div>

        </section>
    )
}