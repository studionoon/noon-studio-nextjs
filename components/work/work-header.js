const List = ({ list }) => {
  if (!list.length) {
    return null;
  }
  return (
    <ul className="list-none">
      {list.map((value, index) => {
        return (
          <li key={index} className="mb-3 text-primary-65">
            {value}
          </li>
        );
      })}
    </ul>
  );
};

const Date = ({ date }) => {
  if (!date) {
    return null;
  }

  return <p className="font-bold">{date}</p>;
};

const WorkHeader = (props) => {
  return (
    <div className="container-small pb-32 pt-56">
      <div className="row">
        <div className="col-12 col-lg-8 order-lg-2 mb-4 lg:mb-0">
          <h1 className="sr-only">{props.title}</h1>
          <h6 className="text-accent-100">{props?.client}</h6>
          <p className="font-regular max-w-[100%] text-4xl">{props?.intro}</p>
        </div>
        <div className="col-12 col-lg-4 order-lg-1">
          <Date date={props?.date} />
          <List list={props?.services} />
        </div>
      </div>
    </div>
  );
};

export default WorkHeader;
