import styles from './related-project.module.scss'
import Image from 'next/image'   

export default function RelatedProject() {

    return (

    <>

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container container-small ${styles['col-full']}` }>

                    <div className="row">

                        <div className={ `col-12 col-md-5 ${styles['text-side']}` }>

                            <div className={` ${styles['menu-box']}`}>

                                <a className='' href="/work/reading-museum"><h2 className="h6 has-pseudo-line">Next Project</h2></a>

                                <a className='' href="/work/reading-museum"><h3 className="h4">Reading Museum</h3></a>

                                <a className='text-link' href="/work/reading-museum"><p className="mb-0">Website design <span className={'dash_colour'}></span> development</p></a>

                            </div>

                        </div>

                        <div className={ `col-12 col-md-7 ${styles['image-side']}` }>

                            <a className='' href="/work/reading-museum">

                                <Image
                                    layout="fill"
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />

                            </a>

                        </div>

                    </div>
                
                </div>

            </div>

        </div>

    </>

    )
    
}
