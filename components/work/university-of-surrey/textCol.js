import styles from './mediaText.module.scss'
import Image from 'next/image'


   

export default function WorkTextCol() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['colText']}` }>

                    <div className="row">

                        <h3>The solution</h3>

                        <h4>Developing an immersive platform</h4>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>
                          
                            <p>We started by exploring user journeys from students at on-campus events, looking at how they digested information and how we could replicate this in a virtual format. What we found was that visitors prioritised key aspects of an open day, so as part of the platform we looked to develop an onboarding process which guided visitors to get close to the same experience as at the physical events.</p>

                        </div>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>

                            <p>The platform also allowed integration of live events, which housed live interactive sessions, and subjects pages displayed in a user-friendly way.</p>

                            <p>We provided guidelines which has allowed Reading to implement this visual identity across more of their digital assets.</p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
