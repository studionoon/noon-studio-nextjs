import styles from './mediaText.module.scss'
import Image from 'next/image'

export default function WorkMediaText() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `col-full ${styles['mediaCont']}` }>

                    <div className="row">

                        <div className={ `col-12 col-xl-6 ${styles['mediaCont_text']}` }>

                            <div className={ `${styles['textbg']}` }>

                                <div className={ `${styles['textwrapper']}` }>

                                    <h3>Landing Page Design</h3>

                                    <p>Create a virtual event platform whilst retaining the excitement of Reading’s on-campus events.

                                    Following the outbreak of the Covid-19 pandemic, the University of Reading were looking to bring their Open Days and on-campus events online in a new virtual events format. They wanted to retain the excitement and interactivity that on-campus events delivered.</p>

                                </div>
                            
                            </div>

                        </div>

                        <div className={ `col-12 col-xl-6 ${styles['mediaCont_image']}` }>

                            <div style={{width: '100%', height: '100%', position: 'relative'}}>

                                <Image
                                    layout="fill"
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-143.png"
                                    // height={50}
                                    // width={50}
                                    objectFit='cover'
                                />

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
