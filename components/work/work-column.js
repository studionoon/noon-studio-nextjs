import { Fragment } from "react";
import Image from "next/image";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const WorkHeader = ({ header }) => {
  if (!header) {
    return null;
  }

  return <h3>{header}</h3>;
};

const TextOutput = ({ content }) => {
  return (
    <Fragment>
      {content.map((value, index) => {
        const Tag = `${value.tag}`;
        return (
          <Tag key={index} className={classNames(value?.class, "")}>
            {value.content}
          </Tag>
        );
      })}
    </Fragment>
  );
};

const WorkOneColumn = (props) => {
  const title = props?.title ? props.title : null;
  const content = props?.content ? props.content : null;
  return (
    <div className="container-small pb-16 pt-28 lg:pb-32 lg:pt-56">
      <div className="row">
        <div className="col-12 col-lg-4">
          <h6 className="has-pseudo-line text-primary">{title}</h6>
        </div>
        <div className="col-12 col-lg-8 text-2xl">
          <TextOutput content={content} />
        </div>
      </div>
    </div>
  );
};

const WorkTwoColumn = (props) => {
  const title = props?.title ? props.title : null;
  const content = props?.content ? props.content : null;
  return (
    <div className="container-small pb-16 pt-28 lg:pb-32 lg:pt-56">
      <div className="row">
        <div className="col-12">
          <h6 className="has-pseudo-line text-primary">{title}</h6>
          <WorkHeader {...props} />
          <div className="row">
            {content.map((value, index) => {
              return (
                <div key={index} className="col-12 col-lg-6">
                  <TextOutput content={value} />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

const WorkImage = ({
  image,
  classes = "",
  isFull = true,
  objectPosition = "50% 50%",
}) => {
  if (!image) {
    return null;
  }

  if (!isFull) {
    classes += " container";
  }

  return (
    <div className={classNames(classes, "relative h-60")}>
      <Image
        src={image}
        className="object-cover"
        role="decorative"
        alt=""
        layout="fill"
        objectFit="cover"
        objectPosition={objectPosition}
      />
    </div>
  );
};

const QuoteMark = ({ classes = "" }) => {
  return (
    <span
      className={classNames(classes, "block text-8xl font-bold text-accent")}
    >
      "
    </span>
  );
};

const WorkQuote = ({ quote = {} }) => {
  if (!quote) {
    return null;
  }

  return (
    <div className="bg-primary text-white">
      <div className="container">
        <div className="mx-auto max-w-lg pt-24 pb-20 text-center">
          <QuoteMark classes="leading-6" />
          <TextOutput content={quote.message} />
          <QuoteMark classes="-mb-9" />
          <p className="mb-0 text-xl font-bold">{quote.name}</p>
          <p>{quote.jobtitle}</p>
        </div>
      </div>
    </div>
  );
};

export { WorkQuote, WorkImage, WorkOneColumn, WorkTwoColumn };
