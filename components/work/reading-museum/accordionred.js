import styles from './accordionred.module.scss'
import Image from 'next/image'
import { Navigation, Pagination, Scrollbar, A11y, Mousewheel } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import 'swiper/css/bundle';

export default function WorkAccordion() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['colText']}` }>

                    <div className="row">

                        <h3>2020 - 2021</h3>

                        <h4>The Study of Drawing</h4>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>
                          
                            <p>Tasked with designing an intriguing and entertaining identity, we used a caricature of an art lecturer from Reading which we felt made the marketing conversational whilst not taking itself too seriously. This would make the marketing accessible to the wide target audience, from children through to artists.</p>

                        </div>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>

                            <p>For this exhibition we also worked closely with the team at Reading Museum to design the internal gallery space too, rolling out the exhibition’s identity onto the walls and art captions.</p>

                        </div>

                    </div>
                    <div className={`row museum-swiper`}>

                        <Swiper
                            modules={[Pagination, Mousewheel]}
                            spaceBetween={10}
                            slidesPerView={1.2}
                            loop={true}
                            centeredSlides={true}
                            pagination={{ clickable: true }}
                            mousewheel={true}
                            breakpoints={{
                                640: {
                                slidesPerView: 1.3,
                                spaceBetween: 20,
                                },
                                768: {
                                slidesPerView: 1.6,
                                spaceBetween: 20,
                                }
                            }}
                        >

                            <SwiperSlide>
                                <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                            <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                            <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                            <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>

                        </Swiper>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
