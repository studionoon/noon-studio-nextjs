import styles from './accordionorange.module.scss'
import Image from 'next/image'
import { Navigation, Pagination, Scrollbar, A11y, Mousewheel } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import 'swiper/css/bundle';

export default function WorkAccordion() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['colText']}` }>

                    <div className="row">

                        <h3>2019</h3>

                        <h4>Ladybird Books</h4>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>
                          
                            <p>The museum approached us in 2019 to help promote an exhibition and event celebrating 100 years of Ladybird Books. We were asked to design a visual identity which could be rolled out across physical and digital platforms.</p>

                        </div>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>

                            <p>Using the typeface from Ladybird Books and colours from the family-favourite ‘Ladybird Classic’ books featured in the exhibition, we developed an identity which enticed users and worked across all advertising platforms.</p>

                        </div>

                    </div>
                    <div className={`row museum-swiper`}>

                        <Swiper
                            modules={[Pagination,Mousewheel]}
                            spaceBetween={10}
                            slidesPerView={1.2}
                            loop={true}
                            centeredSlides={true}
                            pagination={{ clickable: true }}
                            mousewheel={true}
                            breakpoints={{
                                640: {
                                slidesPerView: 1.3,
                                spaceBetween: 20,
                                },
                                768: {
                                slidesPerView: 1.6,
                                spaceBetween: 20,
                                }
                            }}
                        >

                            <SwiperSlide>
                                <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/10/SC_Sibos_TMI_homepage_full_Article-1_GettyImages-1279254153_1920x1080@2x.jpg"
                                />
                                {/* <img src="https://api.noon.studio/wp-content/uploads/2022/01/Mask-Group-153.png" /> */}
                            </SwiperSlide>
                            <SwiperSlide>
                            <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                            <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/10/christoph-schulz-ZxFNtfyt5Is-unsplash.jpg"
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                            <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>

                        </Swiper>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
