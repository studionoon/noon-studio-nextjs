import styles from './accordionwhite.module.scss'
import Image from 'next/image'
import { Navigation, Pagination, Scrollbar, A11y, Mousewheel } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import 'swiper/css/bundle';

export default function WorkAccordion() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['colText']}` }>

                    <div className="row">

                        <h3>2020</h3>

                        <h4>Animal World Art Journeys</h4>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>
                          
                            <p>We loved collaborating with the team at Reading Museum and following the success of the Ladybird Exhibition they asked us to help promote an upcoming art exhibition. Designed to appeal to both families and creative thinkers, Animal: World Art Journeys focussed on animals in many forms – from paintings to sculptures and literature.</p>

                        </div>

                        <div className={ `col-12 col-md-5 ${styles['colText_text']}` }>

                            <p>We designed an exhibition identity which would appeal to the audience using striking, collaged imagery of the exhibits to create a coherent visual identity. We then rolled this out to marketing materials for print, digital & advertising running a successful social media strategy to publicise to the target audience.</p>

                        </div>

                    </div>
                    <div className={`row museum-swiper`}>

                        <Swiper
                            modules={[Pagination,Mousewheel]}
                            spaceBetween={10}
                            slidesPerView={1.2}
                            loop={true}
                            centeredSlides={true}
                            pagination={{ clickable: true }}
                            // onSlideChange={() => console.log('slide change')}
                            // onSwiper={(swiper) => console.log(swiper)}
                            mousewheel={true}
                            breakpoints={{
                                640: {
                                slidesPerView: 1.3,
                                spaceBetween: 20,
                                },
                                768: {
                                slidesPerView: 1.6,
                                spaceBetween: 20,
                                }
                            }}
                        >

                            <SwiperSlide>
                                <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                                <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                                <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                                <Image
                                    layout="fill"
                                    // height={1440}
                                    // width={1920}
                                    src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                />
                            </SwiperSlide>

                        </Swiper>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
