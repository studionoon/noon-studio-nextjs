import styles from './quote.module.scss'


export default function WorkQuote() {

    return (

        <div className={ `${styles['wrapper']}` }>

            <div className={styles['content']}>

                <div className={ `container ${styles['WorkQuote']}` }>

                    <div className="row">

                        <div className={ `col-12 ${styles['WorkQuote_text']}` }>
                          
                            <p className={ `${styles['stats_quotemark']}` }>"</p>
                            <p className={ `${styles['stats_quote']}` }>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                            <p className={ `${styles['stats_quotemark_2']}` }>"</p>
                            <p className={ `${styles['stats_name']}` }>Dr Naomi Lebens</p>
                            <p className={ `${styles['stats_title']}` }>Art Curator, Reading Museum</p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    )
    
}
