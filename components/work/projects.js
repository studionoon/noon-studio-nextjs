import styles from './projects.module.scss'
import Image from 'next/image'

export default function WorkProjects( { toShow = 10, showTitle = false } ) {

    return (

        <div className={ `${styles['wrapper']}` }>

            
            <div className={styles['content']}>

                <div className={ `container ` }>

                    { showTitle && 
                    
                        <h6 className={  `has-pseudo-line ${styles['work']}`}>Our latest work</h6>

                    }

                    <div className={ `row ${styles['colprojects']}` }>

                        { toShow >= 1 && 

                            <div className={ `col-12 col-lg-6 ${styles['colproject']}` }>

                                <div className={ `${styles['wrapper']}` }>
                
                                    <div className={ `${styles['content']}` }>

                                        <div className={ `${styles['text_wrapper']}` }>

                                            <h6>Web development</h6>

                                            <h3 className='h4'>Virtual experience platform</h3>

                                            {/* <a className='text-link' href="/work/virtual-reading">Explore</a> */}
                                            <p className='text-link'>Coming soon</p>

                                        </div>

                                        <div className={ `${styles['img_wrapper']}` }>

                                            <Image
                                                // layout="fill"
                                                height={500}
                                                width={400}
                                                src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-3.png"
                                            />

                                        </div>

                                        <a className='white-text stretched-link'></a>

                                    </div>

                                </div>

                            </div>

                        }

                        { toShow >= 2 && 

                        <div className={ `col-12 col-lg-6 ${styles['colproject']} ${styles['even']}` }>

                            <div className={ `${styles['wrapper']}` }>

                                <div className={`${styles['content']}`}>

                                    <div className={`${styles['text_wrapper']}`}>

                                        <h6>Design & Brand</h6>

                                        <h3 className='h4'>Growing for a digital age</h3>

                                        {/* <a href="/work/growing-for-a-digital-age" className='text-link'>Explore</a> */}
                                        <p className='text-link'>Coming soon</p>

                                    </div>

                                    <div className={`${styles['img_wrapper']}`}>

                                        <Image
                                            // layout="fill"
                                            height={500}
                                            width={400}
                                            src="https://api.noon.studio/wp-content/uploads/2022/01/Mask-Group-153.png"
                                        />

                                    </div>

                                    <a className='white-text stretched-link'></a>

                                </div>

                            </div>

                        </div>

                        }

                        { toShow >= 3 && 

                        <div className={ `col-12 col-lg-6 ${styles['colproject']} ${styles['child']}` }>

                        <div className={ `${styles['wrapper']}` }>

                            <div className={ `${styles['content']}` }>

                                <div className={ `${styles['text_wrapper']}` }>

                                    <h6>Digital <span className={ 'has-pseudo-line' }></span> Design & Brand</h6>

                                    <h3 className='h4'>The future of medicine</h3>

                                    {/* <a className='text-link' href="/work/hospitality">Explore</a> */}
                                    <p className='text-link'>Coming soon</p>

                                </div>

                                <div className={ `${styles['img_wrapper']}` }>
                                
                                    <Image
                                        // layout="fill"
                                        height={500}
                                        width={400}
                                        src="https://api.noon.studio/wp-content/uploads/2023/02/Medical-School.jpg"
                                    />

                                </div>

                                <a className='white-text stretched-link'></a>

                            </div>

                        </div>

                        </div>

                        }
                        { toShow >= 4 && 

                            <div className={ `col-12 col-lg-6 ${styles['colproject']} ${styles['even']} ${styles['child']}` }>

                            <div className={ `${styles['wrapper']}` }>

                                <div className={`${styles['content']}`}>

                                    <div className={`${styles['text_wrapper']}`}>

                                        <h6>Design & Brand</h6>

                                        <h3 className='h4'>Exhibitions at Reading Museum</h3>

                                        {/* <a className='text-link' href="/work/reading-museum">Explore</a> */}
                                        <p className='text-link'>Coming soon</p>

                                    </div>

                                    <div className={`${styles['img_wrapper']}`}>

                                        <Image
                                            // layout="fill"
                                            height={500}
                                            width={400}
                                            src="https://api.noon.studio/wp-content/uploads/2022/02/Mask-Group-6.png"
                                        />

                                    </div>

                                    <a className='white-text stretched-link'></a>


                                </div>

                            </div>

                        </div>

                        }


                        { toShow >= 5 && 

                        <div className={ `col-12 col-lg-6 ${styles['colproject']}` }>

                            <div className={ `${styles['wrapper']}` }>

                                <div className={ `${styles['content']}` }>

                                    <div className={ `${styles['text_wrapper']}` }>

                                        <h6>Web development</h6>

                                        <h3 className='h4'>Hospitality</h3>

                                        {/* <a className='text-link' href="/work/virtual-reading">Explore</a> */}
                                        <p className='text-link'>Coming soon</p>

                                    </div>

                                    <div className={ `${styles['img_wrapper']}` }>

                                        <Image
                                            // layout="fill"
                                            height={500}
                                            width={400}
                                            src="https://api.noon.studio/wp-content/uploads/2021/11/resturant-grade-chefs.jpg"
                                        />

                                    </div>

                                    <a className='white-text stretched-link'></a>

                                </div>

                            </div>

                        </div>

                        }   

                        { toShow >= 6 && 

                        <div className={ `col-12 col-lg-6 ${styles['colproject']} ${styles['even']} ${styles['child']}` }>

                        <div className={ `${styles['wrapper']}` }>

                            <div className={`${styles['content']}`}>

                                <div className={`${styles['text_wrapper']}`}>

                                    <h6>Design & Brand</h6>

                                    <h3 className='h4'>Re(a)d all about it</h3>

                                    {/* <a className='text-link' href="/work/reading-museum">Explore</a> */}
                                    <p className='text-link'>Coming soon</p>

                                </div>

                                <div className={`${styles['img_wrapper']}`}>

                                    <Image
                                        // layout="fill"
                                        height={500}
                                        width={400}
                                        src="https://api.noon.studio/wp-content/uploads/2023/02/rh-exhib.jpg"
                                    />

                                </div>

                                <a className='white-text stretched-link'></a>


                            </div>

                        </div>

                        </div>

                        }

                    </div>
                
                </div>

            </div>

        </div>

    )
    
}
