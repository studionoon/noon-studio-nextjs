import Link from 'next/link'
import Container from './container'
import styles from './footer.module.scss'
import { useRouter } from "next/router"
import MenuList from './utils/menu-list'

// Font Awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight } from '@fortawesome/pro-regular-svg-icons'


export default function Footer({ contact, menus }) {

    const router = useRouter();

    const date = new Date();
    const year = date.getFullYear(); 

    const email_jobs = contact?.acfOptionsContact?.contactSettings?.emailJobs;
    const email_default = contact?.acfOptionsContact?.contactSettings?.emailDefault;
    const address = contact?.acfOptionsContact?.contactSettings?.address;
    const addressAlternative = contact?.acfOptionsContact?.contactSettings?.addressAlternative;

    return (


        <>

            { email_default?.length > 0 && 

                <>

                    <section className={ `${styles.git}` }>

                        <a className={styles.gitLink} href={ "mailto:" + email_default } target="_blank">

                            <div className={ `container ${ styles.container }` }>

                                <div className={ `row  ${ styles.row }` }>

                                    <div className={ `col ${ styles.col }` }>

                                        <h2>Start your next<br/> project now</h2>
                                        
                                    </div>

                                    <div className={ `col ${ styles.col }` }>
                                        
                                        <span className={ `col ${ styles.git_cta }` }>Get in touch <FontAwesomeIcon size="xs" icon={faArrowRight} /></span>

                                    </div>
                                    
                                </div>
                                
                            </div>

                        </a>

                    </section>

                </>

            }

            <footer className={styles.footerWrapper}>

                <Container>

                    <div className={styles.inner}>

                        <div className={ `row`}>

                            <div className={ `col ${styles.col }`}>

                            <div className={ `row ${styles.row_left} `}>

                                <div className={ `col ${styles.col } ` }>

                                    { menus?.menu_footer_1.menuItems.edges.length > 0 && <MenuList title="" items={ menus.menu_footer_1.menuItems.edges } /> } 

                                </div>

                                <div className={ `col ${styles.col }  ${styles.hm } ` }>

                                    { menus?.menu_footer_2.menuItems.edges.length > 0 && <MenuList title={ menus.menu_footer_2.name } items={ menus.menu_footer_2.menuItems.edges } /> } 

                                </div>

                            </div>

                            </div>

                            { email_jobs?.length > 0 | email_default?.length > 0  | address?.length > 0 &&
                            
                            <>
                            
                            <div className={ `col ${styles.col }`}>
                            {/* <div className={ `container ${styles.col }`}> */}

                                <div className={ `row ${styles.row_right} `}>

                                    <div className={ `col col-12 col-lg-6 ${styles.col } ` }>

                                    { email_default.length > 0 && 

                                        <>
                                            <h4>Work With Us</h4>
                                            <a className={styles.email} href={ "mailto:" + email_default }>{email_default}</a>
                                        </>
                
                                    }

                                    { email_jobs.length > 0 && 

                                        <>
                                            <h4>Join our team</h4>
                                            <a className={styles.email} href={ "mailto:" + email_jobs }>{email_jobs}</a>
                                        </>

                                    }

                                    </div>

                                    { address.length > 0 && 

                                        <div className={ `col col-12 col-lg-6 ${styles.col }` }>
                                            
                                            <h4>FIND US</h4>
                                            
                                            <a className={styles.directions } href={ "https://goo.gl/maps/CaXU8W52pSx4WL8D6" } target="_blank">
                                                <address className={styles.directions} dangerouslySetInnerHTML={{__html:  address  }}></address>
                                            </a>

                                        </div>

                                    }

                                </div>

                            {/* </div> */}
                            </div>

                            </>
                            
                            }
                            
                        </div>

                    </div>

                    <div className={styles.border}></div>

                    <div className={styles.copyright}>

                        <div className="row">

                            <div className="col">

                            © { contact?.allSettings?.generalSettingsTitle } Limited { year }. All rights reserved.

                            </div>

                        </div>

                    </div>


                </Container>

            </footer>

        </>
    )
}
