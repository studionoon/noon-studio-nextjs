import styles from './our-approach.module.scss'
import Image from 'next/image'
import React, { useState, useEffect } from 'react';
// Font Awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight } from '@fortawesome/pro-solid-svg-icons'

import Link from 'next/link'

const NoonButton = React.forwardRef(({ onClick, href }, ref) => {
    return (
        <a href={href} className='text-link'>
            How we work
        </a>
    )
})

export default function OurApproach({ href }) {

    return (

        <>

            <div className={ `${styles['wrapper']}` }>

                <div className={styles['content']}>

                    <div className={ `container container-small ${styles['col-full']}` }>

                        <div className="row">

                            <div className={ `col-12 col-md-5 ${styles['image-side']}` }>

                            <Image
                                layout="fill"
                                src="https://api.noon.studio/wp-content/uploads/2022/01/Mask-Group-153.png"
                            />

                            </div>

                            <div className={ `col-12 col-md-7 ${styles['text-side']}` }>

                                <div className={` ${styles['menu-box']}`}>

                                    <h3 className="h6 has-pseudo-line mb-4">Our Approach</h3>

                                    <p className="h3">
                                        Discover<br/> <br/>
                                        Design<br/> <br/>
                                        Deliver<br/> <br/>

                                        <Link href='/our-approach/' passHref legacyBehavior>
                                            <NoonButton />
                                        </Link>
                                    </p>

                                </div>

                            </div>

                        </div>
                    
                    </div>

                </div>

            </div>

        </>

    )
    
}
