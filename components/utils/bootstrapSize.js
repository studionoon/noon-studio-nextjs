export default function getBootstrapBreakpoint( size ) {

    // Convert to Lowercase
    let bs_size = size.toLowerCase();

    switch( bs_size ){

        case 'xs':
                return 0;
            break;  
        case 'sm':
                return 576;
            break;  
        case 'md':
                return 768;
            break;  
        case 'lg':
                return 992;
            break;  
        case 'xl':
                return 1200;
            break;  
        case 'xxl':
                return 1500;
            break;  
        default:
            return 9999999999999999;

    }            

}