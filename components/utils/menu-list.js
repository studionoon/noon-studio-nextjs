import { useRouter } from "next/router";
import styles from './menu-list.module.scss'

export default function MenuList({ title, items, menuClass, menuItemClass }) {

    const router = useRouter();
    const current = router.pathname;

    const closeMenu = () => {

        // Create Event
        const event = new Event('close_menu');

        // Dispatch the event.
        window.dispatchEvent(event);

    }

    return (

        <>

            { title.length > 0 && <h4>{ title }</h4> }

            <ul className={menuClass}>

                {items.map((item, i ) => ( // #TODO, not sure on the key being used here
                        
                    <li key={i}>

                        <a href={item.node.path} key={i} onClick={ () => closeMenu() } className={`${menuItemClass} ${styles['menu_item']} ${( current + "/" === item.node.path || router.pathname  === item.node.path || router.pathname === '/[slug]' && '/' + router.query.slug + "/"  == item.node.path ) ? styles.active : "" } ${item?.node?.menuSettings?.itemSize ? styles[ item?.node?.menuSettings?.itemSize ] : ''}`}>{ item.node.label }</a>

                    </li>

                ))}

            </ul>

        </>

    )
}
 
