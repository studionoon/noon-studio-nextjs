import parse, { domToReact } from "html-react-parser";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState, useEffect } from 'react';

const NoonButton = React.forwardRef(({ onClick, href, attribs, is_current, className, children, options, faArrowRight }, ref) => {
    return (
        <a href={href} {...attribs} aria-current={is_current} data-class={className} >{domToReact(children, options)}</a>
    )
})

export default function parseHtml(html, className) {

    const options = {

        replace: ({ name, attribs, children }) => {

        // Convert internal links to Next.js Link components.
        const isInternalLink = name === "a" && attribs["data-internal-link"] === "true";

            if (isInternalLink) {

                const router = useRouter();
                const current = router;
                
                let faArrowRight = '';

                let is_current = false;
                if( "/" + current + "/" == attribs.href ){
                    is_current = true;
                }

                if ( className?.includes('add-arrow-after-text') ) {
                    faArrowRight = <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="arrow-right" className="svg-inline--fa fa-arrow-right fa-w-14 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M216.464 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L387.887 239H12c-6.627 0-12 5.373-12 12v10c0 6.627 5.373 12 12 12h375.887L209.393 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L233.434 36.465c-4.686-4.687-12.284-4.687-16.97 0z"></path></svg>
                }

                return (
                    <a href={attribs.href} aria-current={is_current} className={className} >{domToReact(children, options)}</a>
                );

            }

        },

    };

  return parse(html, options);

}