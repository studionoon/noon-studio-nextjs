import React from "react";

// Grid 
import CoreGroupBlock from "../gutenberg/CoreGroupBlock";
import NoonGroupBlock from "../gutenberg/NoonGroupBlock";
import CoreColumnsBlock from "../gutenberg/CoreColumnsBlock";
import CoreColumnBlock from "../gutenberg/CoreColumnBlock";
import NoonColumnheaderBlock from "../gutenberg/NoonColumnheaderBlock";
import CoreMediaTextBlock from "../gutenberg/CoreMediaTextBlock";

// Text
import CoreHeadingBlock from "../gutenberg/CoreHeadingBlock";
import CoreParagraphBlock from "../gutenberg/CoreParagraphBlock";
import CoreListBlock from "../gutenberg/CoreListBlock";
import CoreListItemBlock from "../gutenberg/CoreListItemBlock";
import CoreButtonsBlock from "../gutenberg/CoreButtonsBlock";
import CoreButtonBlock from "../gutenberg/CoreButtonBlock";

// Images
import CoreImageBlock from "../gutenberg/CoreImageBlock";
import NoonCoverImageBlock from "../gutenberg/NoonCoverImageBlock";
import NoonHeaderImageBlock from "../gutenberg/NoonHeaderImageBlock";
import CoreGalleryBlock from "../gutenberg/CoreGalleryBlock";

// Accordian
import NoonAccordianParentBlock from "../gutenberg/NoonAccordianParentBlock";
import NoonAccordianChildBlock from "../gutenberg/NoonAccordianChildBlock";

// Carousel
import NoonCarouselBlock from "../gutenberg/NoonCarouselBlock";
import NoonCarouselChildBlock from "../gutenberg/NoonCarouselChildBlock";
import NoonCarouselImageBlock from "../gutenberg/NoonCarouselImageBlock";

// Spacer
import CoreSpacerBlock from "../gutenberg/CoreSpacerBlock";


// Partners
import NoonPartners from '../work/partners';
import OurApproach from '../index/home-approach'

// Work
import WorkHeader from '../work/header';
import WorkProjects from '../work/projects';

const Components = {
  
    // Grid
    NoonGroupBlock: NoonGroupBlock,
    CoreGroupBlock: CoreGroupBlock,
    CoreColumnsBlock: CoreColumnsBlock,
    CoreColumnBlock: CoreColumnBlock,
    NoonColumnheaderBlock: NoonColumnheaderBlock,
    CoreMediaTextBlock: CoreMediaTextBlock,
    
    //Text
    CoreParagraphBlock: CoreParagraphBlock,
    CoreHeadingBlock: CoreHeadingBlock,
    CoreListBlock: CoreListBlock,
    CoreListItemBlock: CoreListItemBlock,
    CoreButtonsBlock: CoreButtonsBlock,
    CoreButtonBlock: CoreButtonBlock,

    // Accordian
    NoonAccordianParentBlock: NoonAccordianParentBlock,
    NoonAccordianChildBlock: NoonAccordianChildBlock,

    // Images
    CoreImageBlock: CoreImageBlock,
    NoonCoverImageBlock: NoonCoverImageBlock,
    NoonHeaderImageBlock: NoonHeaderImageBlock,
    CoreGalleryBlock: CoreGalleryBlock,

    // Carousel 
    NoonCarouselBlock: NoonCarouselBlock,
    NoonCarouselChildBlock: NoonCarouselChildBlock,
    NoonCarouselImageBlock: NoonCarouselImageBlock,

    // Spacer
    CoreSpacerBlock: CoreSpacerBlock,

    // Partners
    NoonPartners: NoonPartners,
    NoonApproach: OurApproach,
    HomeApproach: OurApproach,

    // Work
    NoonWorkHeader: WorkHeader,
    NoonWorkProjects: WorkProjects
};

export default block => {
    if (typeof Components[block.__typename] !== "undefined") {
        return React.createElement(Components[block.__typename], {
            key: block._uid,
            block: block
        });
    }
    return React.createElement(
        () => <div className="alert alert-primary">The component <b>{block.__typename}</b> has not been created yet.</div>,
        { key: block._uid }
    );
};
