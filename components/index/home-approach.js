import styles from './home-approach.module.scss'
import Image from 'next/image'
   

export default function OurApproach() {

    return(
        <>
            <div className={ `${styles['wrapper']}` }>

                <div className={styles['content']}>

                    <div className={ `container container-small ${styles['col-full']}` }>

                            <div className="row">

                                <div className={ `col-12 col-md-5 ${styles['image-side']}` }>

                                    <Image
                                        src="https://api.noon.studio/wp-content/uploads/2023/01/our-approach.jpg"
                                        alt="Picture of the author"
                                        layout='fill'
                                    />
                                    
                                </div>

                                <div className={ `col-12 col-md-7 ${styles['text-side']}` }>

                                    <div className={` ${styles['menu-box']}`}>

                                        <h2 className="h6 has-pseudo-line">Our Approach</h2> 

                                        <h3 className="h4">Discover, Design, Deliver.</h3>

                                        <p className="">Every project is unique but our tried and tested approach remains the same and is designed to deliver the most important thing: results for our&nbsp;clients.</p>

                                    <a href='/our-approach/' className='text-link'>How we work</a>

                                </div>

                            </div>
                        
                        </div>

                    </div>

                </div>

            </div>
            

        </>

    )
    
}
