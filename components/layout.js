import Alert from "../components/alert";
import Footer from "../components/footer";
import Meta from "../components/meta";
import Navigation from "./navigation/navigation";
import GoTop from "../components/GoTop";

export default function Layout({ contact, menus, preview, page, children }) {
  return (
    <>
      <Meta page={page} />

      <div className="min-h-screen">
        <Navigation
          page={page}
          contact={contact}
          menuItems={menus.menu_primary}
        />

        <main className="overflow-hidden">
          {children}

          <Footer contact={contact} menus={menus} />
        </main>

        <GoTop />
      </div>
    </>
  );
}
