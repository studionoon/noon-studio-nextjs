import Image from 'next/image'
import styles from './image-overlay.module.scss';

export default function ImageOverlay( { image, text = ''  } ) {

    if( ! image ){
        return null;
    }

    return(

        <div className={ `col ${styles['opacity-overlay']}` } >
                    
                                        
            <Image
                src={image}
            />
                    						
			<div className={ `${styles['image-overlay']}` } style={{ backgroundColor: '#ffc321' }}>

    				<div className={ `${styles['image-overlay-text']}` } style={{ color: '#2b2a29' }}>{ text }</div>

  			</div>
						
             
		</div>

    )

}