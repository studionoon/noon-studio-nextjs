import Components from '../utils/components'
import useWindowDimensions from '../utils/windowSize'
import uniqid from 'uniqid'
import React, { useCallback, useEffect, useState } from 'react'
import { useResizeDetector } from 'react-resize-detector';

import styles from './NoonAccordianParentBlock.module.scss'

export default function NoonAccordianParentBlock( block ) {


    const attr = block.block
    let noon_group = attr.noon_group ? true : false;
    var accordian_id = attr.attributes?.uniqueID ? 'accordian_' + attr.attributes.uniqueID : uniqid();
    var uniqueID = attr.attributes?.uniqueID ? attr.attributes.uniqueID : uniqid();

    const [ widthh, setWidthh ] = useState( 0 );


    const innerBlocks = attr.innerBlocks

    var columns = false
    const display = attr.attributes.accordianDisplay;


    if( display  == 'inline' ){
        noon_group = true
    }

    if(attr.attributes.accordianDisplay == 'columns' ){
        columns = true
    }

    //loop for outer array acting as container
    for(var i = 0; i < innerBlocks.length; i++){
    
        innerBlocks[i].parent = accordian_id;
        innerBlocks[i].uniqueID = uniqueID + '_' + i;
        innerBlocks[i].width = widthh;
        innerBlocks[i].breakpoint = attr.attributes.accordion;
        innerBlocks[i].addNumbers = attr.attributes.addNumbers;
        innerBlocks[i].columns = columns;
        innerBlocks[i].display = display;
        innerBlocks[i].iteration = i;

    } 

    const onResize = useCallback((width) => {

        // on resize logic
        setWidthh( width );

    }, []);
    
    const { width, height, ref } = useResizeDetector({
        handleHeight: false,
        refreshMode: 'undefined',
        refreshRate: 500,
        onResize

    });

    return ( 

        <>


            { ! noon_group &&


                <div className={ `wrapper ${ [attr.attributes.colorName] }`} ref={ref}>

                    <div className={ `container` }>

                        <div className="accordion" id={accordian_id}  >

                            <div className="row">
                    
                                { innerBlocks && 

                                    <>

                                        { innerBlocks.map( ( block, index ) => {

                                            return (

                                                <React.Fragment key={index} >

                                                    {Components( block, index ) }

                                                </React.Fragment>

                                            );

                                        } ) }

                                    </>

                                }

                            </div>

                        </div>
                
                    </div>

                </div>

            }

            { noon_group && display == 'with-images' &&

                <div className={ `accordion ${ styles['accordion'] } ${ styles[display] }` } id={accordian_id}  >

                    <div className="row">

                        <div className="col-12 col-md-8 col-lg-6 col-xl-5">

                            { innerBlocks && 

                                <>

                                    { innerBlocks.map( ( block, index ) => {

                                        return (

                                            <React.Fragment key={index} >

                                                {Components( block, index ) }

                                            </React.Fragment>

                                        );

                                    } ) }

                                </>

                            }

                        </div>
                        <div className="col-12 col-md-4 col-lg-6 col-xl-7" id={ 'img_' + accordian_id }>

                        </div>
                    </div>

                </div>

            }

            { noon_group && display != 'with-images' &&

                <div className={ `accordion ${ styles.accordion }` } id={accordian_id}  >

                    <div className="row">

                        { innerBlocks && 

                            <>

                                { innerBlocks.map( ( block, index ) => {

                                    return (

                                        <React.Fragment key={index} >

                                            {Components( block, index ) }

                                        </React.Fragment>

                                    );

                                } ) }

                            </>

                        }

                    </div>

                </div>

            }

        </>
        
    )

}