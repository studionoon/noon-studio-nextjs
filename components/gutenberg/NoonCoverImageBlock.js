import Image from 'next/image'
import styles from './NoonCoverImageBlock.module.scss'
export default function NoonCoverImageBlock( block ) {

    const attr = block.block

    // Focal Point
    const focalPoint = attr.attributes.focalPoint ? JSON.parse( attr.attributes.focalPoint ) : undefined;
    const objectPosition = focalPoint ? `${ Math.round( focalPoint.x * 100 ) }% ${ Math.round( focalPoint.y * 100 ) }%` : undefined;

    return ( 

        <>

            <div className={ `wrapper ${styles['fullwidth-image']}` }>

                <Image
                    src={attr.attributes.mediaURL}
                    alt="Picture of the author"
                    layout='fill'
                    objectFit='cover'
                    objectPosition= { objectPosition }
                    
                /> 

            </div>
        
        </>
    
    )

}
  