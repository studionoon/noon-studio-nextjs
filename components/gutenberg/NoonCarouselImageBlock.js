import Image from 'next/image'
import uniqid from 'uniqid';

export default function NoonCarouselImageBlock( block ) {

    const attr = block.block
    const url = attr?.attributes?.mediaURL ? attr.attributes.mediaURL : undefined;
    const parent_id = uniqid()
    const imageBreakpoints = attr.imageBreakpoints
    const imageBreakpointUnits = attr.imageBreakpointUnits
    
    const focalPoint = attr.attributes.focalPoint ? JSON.parse( attr.attributes.focalPoint ) : undefined;
    const objectPosition = focalPoint ? `${ Math.round( focalPoint.x * 100 ) }% ${ Math.round( focalPoint.y * 100 ) }%` : undefined;

    let test = 'div.' + parent_id + '{ position: relative; width: 100%; display: inline-block; float: left; margin-bottom:10px; }';

    Object.keys( imageBreakpoints ).forEach( key =>{

        var height = imageBreakpoints[key] ? imageBreakpoints[key] : '100';
        var unit = imageBreakpointUnits[key] ? imageBreakpointUnits[key] : 'px';

        test += ' @media (min-width: ' + key + 'px ) { div.'+ parent_id + '{ height:'  +  height + unit + '} }'
        
    })

    return ( 

        <>

            <div className={parent_id}>

                { url &&

                    <Image
                        src={url}
                        alt="Picture of the author"
                        layout='fill'
                        objectFit='cover'
                        objectPosition= { objectPosition }
                        
                    />

                } 

                <style>{test}</style>

            </div>

        </>
        
    )

}