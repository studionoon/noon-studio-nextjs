import Components from '../utils/components'
import styles from './CoreGroupBlock.module.scss'
import parseHtml from "../utils/parser";
import React, { useState, useEffect } from 'react';

export default function NoonGroupBlock( block ) {

    const attr = block.block

    const groupTitle = attr?.attributes?.groupTitle? parseHtml( attr.attributes.groupTitle ) : undefined;
    const groupIntro= attr?.attributes?.groupIntro? parseHtml( attr.attributes.groupIntro ) : undefined;

    let classes = '';

    // Varibles  
    classes += attr?.attributes?.backgroundColor ? ' ' + attr.attributes.backgroundColor : ''
    classes += attr?.attributes?.textColor ?  ' text-' + attr.attributes.textColor : ''
    classes += attr?.attributes?.className ?  ' ' +attr.attributes.className : ''

    // Spacing
    classes += ( attr.attributes.groupPadding  == 'none' ) ? ' p-0' : ' ' + styles['p-' + attr.attributes.groupPadding]

    // Inner blocks
    const innerBlocks = attr.innerBlocks
    
    //loop for outer array acting as container
    for(var i = 0; i < innerBlocks.length; i++){
    
        innerBlocks[i].noon_group = true;

    } 
    
    return ( 
    
        <div className={ `wrapper wrapper-noon-group${ classes }` }>

            <div className={ `container container-${attr.attributes.groupSize}` }>

                { groupTitle && 
                
                    <div className={ `row` }>

                        <div className={ `col-12` }>
                            
                            { attr.attributes.addTitle &&

                                <h2 className={ `${ styles['group-title'] }  ${styles[attr.attributes.titleSize]}  `} >{ groupTitle }</h2>
                    
                            }
                            
                            { attr.attributes.addIntro &&

                            <p className={ `${ styles['group-intro'] }`} >{groupIntro}</p>

                            }
                        
                        </div>

                </div> 
                
                }
            
                { innerBlocks && 

                    <>

                        { innerBlocks.map( ( block, index ) => {

                            return (

                                <React.Fragment key={index} >

                                    {Components( block, index ) }

                                </React.Fragment>

                            );
                            
                        } ) } 

                    </>

                }

            </div>

        </div>
    
    )

}