// Resize
import { useEffect, useState } from 'react'

export default function CoreSpacerBlock( block ) {

    const attr = block.block
    let attributes  = JSON.parse(  attr.attributesJSON );
    let height = attributes.height;

    const [ spacerHeight, setSpacerHeight ] = useState( height );


    useEffect(() => {

        // Update the document title using the browser API
        window.addEventListener('resize', function(event) {

            if( document.body.clientWidth < 576 && attr.attributes.heightXS !== 0 ){

               height = attributes.heightXS;

            }
            else if( ( document.body.clientWidth >= 576 && document.body.clientWidth < 768  )&& attr.attributes.heightSM !== 0 ){

               height = attributes.heightSM;

            }
            else if( ( document.body.clientWidth >= 768 && document.body.clientWidth < 992  )&& attr.attributes.heightMD !== 0 ){

               height = attributes.heightMD;

            }
            else if( ( document.body.clientWidth >= 992 && document.body.clientWidth < 1200  )&& attr.attributes.heightLG !== 0 ){

               height = attributes.heightLG;

            }
            else if( ( document.body.clientWidth >= 1200 && document.body.clientWidth < 1400  )&& attr.attributes.heightXL !== 0 ){

               height = attributes.heightXL;

            }
            else if( ( document.body.clientWidth >= 1400   )&& attributes.heightXXL !== 0 ){

               height = attributes.heightXXL;

            }             
            
            if( height == null || height == 0 ){

               height =  attributes.height;

            }
           

            setSpacerHeight( height );

        });

        window.addEventListener('DOMContentLoaded', function(event) {

            if( document.body.clientWidth < 576 && attr.attributes.heightXS !== 0 ){

               height = attributes.heightXS;

            }
            else if( ( document.body.clientWidth >= 576 && document.body.clientWidth < 768  )&& attr.attributes.heightSM !== 0 ){

               height = attributes.heightSM;

            }
            else if( ( document.body.clientWidth >= 768 && document.body.clientWidth < 992  )&& attr.attributes.heightMD !== 0 ){

               height = attributes.heightMD;

            }
            else if( ( document.body.clientWidth >= 992 && document.body.clientWidth < 1200  )&& attr.attributes.heightLG !== 0 ){

               height = attributes.heightLG;

            }
            else if( ( document.body.clientWidth >= 1200 && document.body.clientWidth < 1400  )&& attr.attributes.heightXL !== 0 ){

               height = attributes.heightXL;

            }
            else if( ( document.body.clientWidth >= 1400   )&& attributes.heightXXL !== 0 ){

               height = attributes.heightXXL;

            } 

            if( height == null || height == 0 ){

               height =  attributes.height;

            }

            setSpacerHeight( height );

        });


      });



    return ( 
        
        <>
        
            <div style={ { height: spacerHeight } }></div>

        </>
        
    )

}