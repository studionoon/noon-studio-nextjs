import Image from 'next/image'
import Components from '../utils/components'
import styles from './NoonHeaderImageBlock.module.scss'
import parseHtml from "../utils/parser";
import React, { useState, useEffect } from 'react';

export default function NoonHeaderImageBlock( block ) {

    const attr = block.block

    const backgroundColor = attr.attributes.backgroundColor ? ' ' + attr.attributes.backgroundColor : ''
    const textColor = attr.attributes.textColor ?  ' text-' + attr.attributes.textColor : ''

    // Classes
    const classes = textColor + backgroundColor;

    // Focal Point
    const focalPoint = attr.attributes.focalPoint ? JSON.parse( attr.attributes.focalPoint ) : undefined;
    const objectPosition = focalPoint ? `${ Math.round( focalPoint.x * 100 ) }% ${ Math.round( focalPoint.y * 100 ) }%` : undefined;

    const headerimageHeading  = parseHtml( attr.attributes.headerimageHeading );
    const headerimageTitle  = parseHtml( attr.attributes.headerimageTitle );

    return ( 

        <>

            <div className={ `wrapper ${ classes } ${ styles['NoonHeader__Block'] }` }>

                <div className="container">

                    <div className="row">

                        <div className={ `col-12 col-lg-7 col-xxl-5 ${ styles['text_wrapper'] } `}>

                            <h2 className='h6'>{headerimageHeading}</h2>
                            <h1>{headerimageTitle}</h1>

                            { attr.innerBlocks && 

                                <>

                                    { attr.innerBlocks.map( ( block, index ) => {

                                        return (

                                            <React.Fragment key={index} >

                                                {Components( block, index ) }

                                            </React.Fragment>

                                        );

                                    } ) } 

                                </>

                            }

                        </div>

                        <div className={`col-12 col-lg-5 col-xxl-6`}>
                            
                            <div className={ `${ styles['image_wrapper'] }`}>

                                <Image
                                    src={attr?.attributes?.mediaURL}
                                    alt="Heading Picture"
                                    layout='fill'
                                    objectFit='cover'
                                    objectPosition= { objectPosition }
                                /> 

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        
        </>
    
    )

}
  