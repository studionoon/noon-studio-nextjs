import styles from './CoreButtonsBlock.module.scss'
import Components from '../utils/components'


export default function CoreButtonsBlock( block ) {

    const attr = block.block

    return ( 
        
        <>

            <a href={ attr.attributes.url } className={ `btn btn-primary btn-lg ${ attr.attributes.className }` } tabIndex="-1" role="button" aria-disabled="true">{ attr.attributes.text }</a>

        </>
        
    )

}