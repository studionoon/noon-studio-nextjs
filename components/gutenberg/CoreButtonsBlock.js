import styles from './CoreButtonsBlock.module.scss'
import Components from '../utils/components'
import React, { useState, useEffect } from 'react';


export default function CoreButtonsBlock( block ) {

    const attr = block.block

    return ( 
        <>

        <div>

            { attr.innerBlocks && 

                <> 
                
                    { attr.innerBlocks.map( ( block, index ) => {

                        return (
                            
                            <React.Fragment key={index} >

                                {Components( block, index ) }

                            </React.Fragment>

                        );

                    } ) }
                
                </>

            }

        </div>

        </>
        
    )

}