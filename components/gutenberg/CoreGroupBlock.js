import Components from '../utils/components'
import styles from './CoreGroupBlock.module.scss'
import React, { useState, useEffect } from 'react';

export default function CoreGroupBlock( block ) {

    const attr = block.block

    const groupTitle = attr?.attributes?.groupTitle;

    // Varibles  
    const backgroundColor = attr.attributes.backgroundColor ? ' ' + attr.attributes.backgroundColor : ''
    const textColor = attr.attributes.textColor ?  ' text-' + attr.attributes.textColor : ''

    // Classes
    const classes = textColor + backgroundColor;
    
    return ( 
    
        <div className={ `wrapper${ classes }` }>

            <div className={ `container` }>

                { groupTitle && 
                
                    <div className={ `row` }>

                        <div className={ `col-12` }>

                            <h3 className={ `${ styles['group-title'] }`} >{ groupTitle }</h3>
                        
                        </div>

                    </div> 
                
                }
            
                { attr.innerBlocks && 

                    <>

                        { attr.innerBlocks.map( ( block, index ) => {

                            return (

                                <React.Fragment key={index} >

                                    {Components( block, index ) }

                                </React.Fragment>

                            );

                        } ) }

                    </>

                }
        
            </div>

        </div>
    
    )

}