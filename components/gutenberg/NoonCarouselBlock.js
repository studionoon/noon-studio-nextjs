import Components from '../utils/components'
import styles from './CoreGroupBlock.module.scss'
import parseHtml from "../utils/parser";
import getBootstrapBreakpoint from '../utils/bootstrapSize'

// Resize
import { useCallback, useEffect, useState } from 'react'
import { useResizeDetector } from 'react-resize-detector';

// Import Swiper React components
import { Navigation, Pagination, Scrollbar, A11y, FreeMode, Mousewheel } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';

export default function NoonCarouselBlock( block ) {

    const attr = block.block

    const groupTitle = attr?.attributes?.groupTitle;
    const groupIntro= attr.attributes?.groupIntro ? parseHtml( attr.attributes.groupIntro ) : undefined;
    const groupCTA= attr.attributes?.groupCTA ? parseHtml( attr.attributes.groupCTA ) : undefined;

    // Varibles  
    const backgroundColor = attr?.attributes?.backgroundColor ? ' ' + attr.attributes.backgroundColor : ''
    const textColor = attr?.attributes?.textColor ?  ' text-' + attr.attributes.textColor : ''

    // Classes
    const classes = textColor + ' ' + backgroundColor;

    // Noon Group
    const inside_noonGroup = block.block?.noon_group ? true : false;
    
        // Slider

            //Breakpoints
            const breakpoints = attr.attributes?.breakpoints ?  JSON.parse( attr.attributes.breakpoints ): undefined;
            if( breakpoints ){
                Object.entries(breakpoints).map(([key, value]) =>  ( breakpoints[ getBootstrapBreakpoint( key ) ] = {  slidesPerView  : value} , delete breakpoints[key]   ) );
            }
            const imageBreakpoints = attr.attributes?.imageBreakpoints ?  JSON.parse( attr.attributes.imageBreakpoints ): undefined;
            if( imageBreakpoints ){
                Object.entries(imageBreakpoints).map(([key, value]) =>  ( imageBreakpoints[ getBootstrapBreakpoint( key ) ] = value , delete imageBreakpoints[key]   ) );
            }
            const imageBreakpointUnits = attr.attributes?.imageBreakpointUnits ?  JSON.parse( attr.attributes.imageBreakpointUnits ): undefined;
            if( imageBreakpointUnits ){
                Object.entries(imageBreakpointUnits).map(([key, value]) =>  ( imageBreakpointUnits[ getBootstrapBreakpoint( key ) ] = value , delete imageBreakpointUnits[key]   ) );
            }

    // Work out Widths

    const [ windowWidth, setWindowWidth ] = useState( 0 );

    const onResize = useCallback((width) => {

        // on resize logic
        setWindowWidth( width );

        }, []);
    
    const { width, height, ref } = useResizeDetector({
        handleHeight: false,
        refreshMode: 'undefined',
        refreshRate: 500,
        onResize
    });


    // Inner blocks
    const innerBlocks = attr.innerBlocks
    //loop for outer array acting as container
    for(var i = 0; i < innerBlocks?.length; i++){
    
        innerBlocks[i].window_width = windowWidth;
        innerBlocks[i].imageBreakpoints = imageBreakpoints;
        innerBlocks[i].imageBreakpointUnits = imageBreakpointUnits;

    } 

    return ( 

        <>
    
            { inside_noonGroup && 

                <>

                    <div ref={ref}>


                        <Swiper
                        
                            modules={[FreeMode, Navigation, Pagination, Scrollbar, A11y, Mousewheel]}
                            spaceBetween={20}
                            slidesPerView={1.2}
                            scrollbar={{ draggable: true }}
                            breakpoints= { breakpoints }
                            mousewheel={true}
                        >

                            { attr.innerBlocks && 

                                <>

                                { attr.innerBlocks.map( (block, i) => (

                                    <SwiperSlide key={i}>

                                        { Components( block ) } 

                                    </SwiperSlide>
                                    
                                ) ) }

                                </>

                            }

                        </Swiper>

                        { groupCTA && 
                        
                            <div className={ `row` }>

                                <div className={ `col-12` }>

                                    { groupCTA && <p className={ `${ styles['group-intro'] }`} >{ groupCTA }</p> }
                                
                                </div>

                            </div> 

                        }  

                    </div>

                </>
            
            } 

            { ! inside_noonGroup && 

                <div className={ `wrapper${ classes }` } ref={ref}>

                    <div className={ `container` }>
            
                        { groupTitle && 
                        
                            <div className={ `row` }>
            
                                <div className={ `col-12` }>
            
                                    <h2 className={ `${ styles['group-title'] }`} >{groupTitle}</h2>
            
                                    { groupTitle && 
                                    
                                        <p className={ `${ styles['group-intro'] }`} >{ groupIntro }</p> 
                                        
                                    }
                                
                                </div>
            
                        </div> 
                        
                        }
            
                        <Swiper
                        
                            // install Swiper modules
                            modules={[FreeMode, Navigation, Pagination, Scrollbar, A11y, Mousewheel]}
                            spaceBetween={50}
                            slidesPerView={1.2}
                            scrollbar={{ draggable: true }}
                            breakpoints= { breakpoints }
                            mousewheel={true}
                        >
                    
                            { attr.innerBlocks && 
            
                                <>
            
                                    { attr.innerBlocks.map( (block, i) => (

                                        <SwiperSlide key={i}>

                                            { Components( block ) } 

                                        </SwiperSlide>

                                    ) ) }
            
                                </>
            
                            }
            
                        </Swiper>
            
                        { groupCTA && 
                        
                            <div className={ `row` }>
            
                                <div className={ `col-12` }>
            
                                    { groupCTA && <p className={ `${ styles['group-intro'] }`} >{ groupCTA }</p> }
                                
                                </div>
            
                            </div> 
                    
                        }  
            
                    </div>
        
                </div>
        
            }

        </>
    
    )

}