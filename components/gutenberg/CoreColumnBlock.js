import Components from '../utils/components'
import React, { useState, useEffect } from 'react';


export default function CoreColumnsBlock( block ) {

    const attr = block.block
    const innerBlocks = attr.innerBlocks

    //loop for outer array acting as container
    for(var i = 0; i < innerBlocks.length; i++){
    
        innerBlocks[i].noon_group = true;

    } 

    return ( 
    
        <div className={ `col-auto ${ attr?.attributes?.className }` }>

            { innerBlocks && 

                <>

                    { innerBlocks.map( ( block, index ) => {

                        return (
                            
                            <React.Fragment key={index} >

                                {Components( block, index ) }

                            </React.Fragment>

                        );
                        
                    } ) } 

                </>

            }

        </div>

    )

  }