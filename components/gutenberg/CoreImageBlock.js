import Image from 'next/image'
import styles from './CoreImageBlock.module.scss'

import _JSXStyle from 'styled-jsx/style'
import { faSledding } from '@fortawesome/pro-regular-svg-icons';


export default function CoreImageBlock( block ) {

    const attr = block.block
    const url = attr?.attributes?.url ? attr.attributes.url : undefined;
    const imageSize = attr?.attributes?.useImageSize ? attr.attributes.useImageSize : false;

    return ( 

        <>

            { url && imageSize &&       
            
                <div className={ `${ styles['div-image-wrapper'] }`} >

                    <Image
                        src={url}
                        alt="Picture of the author"
                        height={attr?.attributes?.height}
                        width={attr?.attributes?.width}
                    />

                </div>

            } 
            { url && !imageSize &&

                <div className={ `${ styles['div-img-wrapper'] }`} >

                    <Image
                        src={url}
                        alt="Picture of the author"
                        layout='fill'
                    />

                </div>

            } 
        
        </>
        
    )

}