import styles from './CoreMediaTextBlock.module.scss'
import Components from '../utils/components'
import Image from 'next/image'
import getBootstrapBreakpoint from '../utils/bootstrapSize'
import React, { useState, useEffect } from 'react';
import uniqid from 'uniqid';

export default function CoreMediaTextBlock( block ) {

    const attr = block.block
    const url =  attr.attributes.mediaUrl

    var media_id = attr.attributes.uniqueID ? 'mb_' + attr.attributes.uniqueID : uniqid();
    var mediaWidth = attr?.attributes?.mediaWidth ? attr.attributes.mediaWidth + '%' : 'auto';
    const noon_group = attr.noon_group ? true : false;

    const focalPoint = attr.attributes.focalPoint ? JSON.parse( attr.attributes.focalPoint ) : undefined;
    const objectPosition = focalPoint ? `${ Math.round( focalPoint.x * 100 ) }% ${ Math.round( focalPoint.y * 100 ) }%` : undefined;

    // Styling 
    const imageBreakpoints = attr.attributes.imageBreakpoints ?  JSON.parse( attr.attributes.imageBreakpoints ): undefined;
    if( imageBreakpoints ){
        Object.entries(imageBreakpoints).map(([key, value]) =>  ( imageBreakpoints[ getBootstrapBreakpoint( key ) ] = value , delete imageBreakpoints[key]   ) );
    }
    const imageBreakpointUnits = attr.attributes.imageBreakpointUnits ?  JSON.parse( attr.attributes.imageBreakpointUnits ): undefined;
    if( imageBreakpointUnits ){
        Object.entries(imageBreakpointUnits).map(([key, value]) =>  ( imageBreakpointUnits[ getBootstrapBreakpoint( key ) ] = value , delete imageBreakpointUnits[key]   ) );
    }

    // Allows you to align the text in the media text block to be center, top or bottom aligned.
    const verticalAlignment = attr.attributes.verticalAlignment
    let vAlign = ""
    if ( verticalAlignment == "center" ) {
        vAlign = "is-vertically-aligned-center"
    } else if ( verticalAlignment == "top" ) {
        vAlign = "is-vertically-aligned-top"
    } else if ( verticalAlignment == "bottom" ) {
        vAlign = "is-vertically-aligned-bottom"
    }

    let style  = 'div.con_' + media_id + '{ height:100%;  }';
    style += 'div.' + media_id + '{ z-index:3; position: relative; width: 100%; display: inline-block; float: left; height:100%; }';

    Object.keys( imageBreakpoints ).forEach( key =>{

        var height = imageBreakpoints[key] ? imageBreakpoints[key] : '100';
        var unit = imageBreakpointUnits[key] ? imageBreakpointUnits[key] : 'px';

        style += ' @media (min-width: ' + key + 'px ) { div.'+media_id+'{ min-height:'  +  height + unit +' !important} }'
        style += ' @media (min-width: ' + key + 'px ) { div.con_'+media_id+'{ min-height:'  +  height + unit +' !important} }'
        
    })

    let halfWidthimgClass = attr.attributes.halfWidthimg;

    let mediaPositionVar = attr.attributes.mediaPosition
    let mediaPosition = "order-1"
    let textPosition = "order-2"

    if ( mediaPositionVar == "right" ) {
        mediaPosition = "order-lg-2"
        textPosition = "order-l-1"
    }


    const Variblebg = attr.attributes.addBg ?  `div` : `span`;  


    return ( 
        <>

       { noon_group && 

            <div className={`row text-${attr.attributes.textColor }`}>

                <div className={`col-12 col-lg-6 position-relative ${ mediaPosition } ${styles[mediaPositionVar]} ${ (attr.attributes.halfPage) ? styles['halfPageImage'] : 'col-xl-7' } `}> {/* style= { {  width : mediaWidth } } */ } 

                    { url &&

                        <>

                            <div className={ media_id }>

                                <Image
                                    src={url}
                                    alt="Picture of the author"
                                    layout='fill'
                                    objectFit='cover'
                                    objectPosition= { objectPosition }
                                />

                                <style>{style}</style>

                            </div>

                        </>

                    }

                    { attr.attributes.addBg &&

                    <div className={` ${styles.image_overlap_bg} ${attr.attributes.backgroundColor} text-${attr.attributes.textColor } `}></div>

                    }

                </div>

                <div className={`col-12 col-lg-6 col-xl-5 position-relative ${ textPosition } ${ styles[vAlign] } ${styles.textContent} ${styles[attr.attributes.addBg]}` }>

                    <Variblebg className={ styles.image_bg_z }>

                    {  noon_group }

                    { attr.innerBlocks && 

                        <> 
                     

                            { attr.innerBlocks.map( ( block, index ) => {

                                return (

                                    <React.Fragment key={index} >

                                        {Components( block, index ) }

                                    </React.Fragment>
  
                                );

                            } ) }                    
                     
                        </>

                    }

                    { attr.attributes.addBg &&

                        <div className={ ` ${styles.image_overlap_bg } ${attr.attributes.backgroundColor} text-${attr.attributes.textColor } `} ></div>

                    }

                    </Variblebg>  
       
                </div>

            </div>

        }

        { ! noon_group && 

        <div className="wrapper ">

            To be built

        </div>

        }

        </>
        
    )

}