import parseHtml from "../utils/parser";

export default function CoreListItemBlock( block ) {

    const attr = block.block
    const li_content =attr?.attributes?.content? parseHtml( attr.attributes.content ) : undefined;

    if( ! li_content ){
        return null;
    }

    return ( 
        <li>{ li_content }</li>
    )

}
  

