import styles from './CoreListBlock.module.scss'
import React from 'react';
import Components from '../utils/components'

export default function CoreListBlock( block ) {

    const attr = block.block

    // Varibles  
    const showColumnsOnMobile = attr.attributes.showColumnsOnMobile ?  styles['list--mobile-collapse'] : ''
    const listColumns = attr.attributes.listColumns ?  styles['list--' + attr.attributes.listColumns + '-columns'] : ''

    // Classes
    const listClasses = showColumnsOnMobile + ' ' + listColumns;

    return ( 

        <>
    
            <ul className={ `
                ${ attr.attributes.hideTickIcon? styles.noonStyledHideTick : '' } 
                ${ attr.attributes.addListCheckMark?  '' : styles.noonStyled }
                ${ listClasses }` } >

                { attr.innerBlocks && 

                    <> 

                        { attr.innerBlocks.map( ( block, index ) => {

                            return (
                                
                                <React.Fragment key={index} >

                                    {Components( block, index ) }

                                </React.Fragment>

                            );

                        } ) }

                    </>

                }

            </ul>

        </>
        
    )

}
  

