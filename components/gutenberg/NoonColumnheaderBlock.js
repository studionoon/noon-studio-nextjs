import Components from '../utils/components'
import styles from './NoonColumnheaderBlock.module.scss'
import parseHtml from "../utils/parser";

export default function NoonColumnheaderBlock( block ) {

    const attr = block.block

    const groupTitle = attr?.attributes?.groupTitle? parseHtml( attr.attributes.groupTitle ) : undefined;
    const groupIntro= attr?.attributes?.groupIntro? parseHtml( attr.attributes.groupIntro ) : undefined;
    
    return ( 
    
        <div className={ `wrapper p-0` }>
    
            { groupTitle && 
            
                <div className={ `row` }>
                        
                    { attr.attributes.addTitle &&

                        <h4 className={ `${ styles['group-title'] } ${attr?.attributes?.className}  ${styles[attr.attributes.titleSize]}  `} >{ groupTitle }</h4>

                    }
                    
                    { attr.attributes.addIntro &&

                        <p className={ `${ styles['group-intro'] }`} >{groupIntro}</p>

                    }

                </div> 
            
            }
    
        </div>
        
    )
    
}