import Components from '../utils/components'
import styles from './CoreGalleryBlock.module.scss'
import Image from 'next/image'

export default function CoreGalleryBlock( block ) {

    const attr = block.block

    // Varibles  
    const backgroundColor = attr.attributes.backgroundColor ? ' ' + attr.attributes.backgroundColor : ''
    const textColor = attr.attributes.textColor ?  ' text-' + attr.attributes.textColor : ''

    // Classes
    const classes = textColor + backgroundColor;

    // Images Loop
    const imagesData = attr?.attributes?.images; // is an object
    
    return ( 
    
        <div className={ `row ${ styles['Noonlogo__Block'] }` }>

            <div className={ `col-12 ${ styles['logo-wrapper'] }` }>

                <div className={ `row justify-content-center` }>

                    {imagesData.map((item, i) => (
                        
                        <div key={i} className={ `col` }>

                            <div className={ `${ styles['logo-image'] }`}>

                                <Image
                                    src={item.url}
                                    alt={item.alt}
                                    layout="fill"
                                    objectFit="contain"
                                />

                            </div>

                        </div>

                    ))}
            
                </div>

            </div> 

        </div> 
    
    )

  }