import Components from '../utils/components';
import React, { useState, useEffect } from 'react';

// Import Swiper styles
export default function NoonCarouselChildBlock( block ) {

    const attr = block.block
    const attributes =  attr.attributes
    const window_width = attr.window_width
    const imageBreakpoints = attr.imageBreakpoints
    const imageBreakpointUnits = attr.imageBreakpointUnits

    // Inner blocks
    const innerBlocks = attr.innerBlocks
    //loop for outer array acting as container
    for(var i = 0; i < innerBlocks.length; i++){
    
        innerBlocks[i].window_width = window_width;
        innerBlocks[i].imageBreakpoints = imageBreakpoints;
        innerBlocks[i].imageBreakpointUnits = imageBreakpointUnits;

    } 

    return ( 

        <div>

            { attr.innerBlocks && 

                <>

                    { attr.innerBlocks.map( ( block, index ) => {

                        return (

                            <React.Fragment key={index} >

                                {Components( block, index ) }

                            </React.Fragment>

                        );
                        
                    } ) }

                </>

            }

        </div>

    )

}