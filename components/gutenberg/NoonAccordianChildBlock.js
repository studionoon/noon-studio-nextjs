import Components from '../utils/components'
import getBootstrapBreakpoint from '../utils/bootstrapSize'
import React, { useState, useEffect } from 'react';
import styles from './NoonAccordianChildBlock.module.scss'
import parseHtml from "../utils/parser";

export default function NoonAccordianChildBlock( block ) {

    const attr = block.block
    const attributes =  attr.attributes
    const title = attr.attributes.acccordianHeader;

    const heading_id = 'accordian-' + attr.uniqueID;
    const collapse_id = 'collapse-' + attr.uniqueID;

    const display = attr.display

    let iteration = attr.iteration
    let i = attr.iteration
    let numbers = attr.iteration + 1;

    if( iteration % 2 === 0){

        iteration = true;

    } else {

        iteration = false;
            
    }

    const columns = attr.columns;
    if( ! columns ){ attr.breakpoint = 'none' }

    const [accordian, setAccordian] = useState( attr.width );

    const breakpoint = getBootstrapBreakpoint( attr.breakpoint );

    const mediaURL1 = attributes.mediaURL1;
    const mediaURL2 = attributes.mediaURL2;

    let classes = 'col';

    // Column Classes 
    if( attributes.columnsXXL ){

        classes += " col-xxl-" + attributes.columnsXXL; 

    } 

    if( attributes.columnsXL ){

        classes += " col-xl-" + attributes.columnsXL; 

    } 

    if( attributes.columnsLG ){

        classes += " col-lg-" + attributes.columnsLG; 

    }

    if( attributes.columnsMD ){

        classes += " col-md-" + attributes.columnsMD; 

    }

    if( attributes.columnsSM ){

        classes += " col-sm-" + attributes.columnsSM; 

    }

    if( attributes.columnsXS ){

        classes += " col-" + attributes.columnsXS; 

    } 

    const [ images, setImages ] = useState();

    useEffect( () => { 

        setAccordian( attr.width );

        // if with-images and .show
        if( display == 'with-images'  ){

            var oldParent = document.getElementById( "additonal_" + collapse_id );
            var newParent = document.getElementById( "img_" + attr.parent );


            let collapse = document.getElementById( collapse_id );
            collapse.addEventListener('shown.bs.collapse', function () {

                newParent.innerHTML = oldParent?.innerHTML;

            });

            if( i == 0 ){

                newParent.innerHTML = oldParent?.innerHTML;
                
            }

        }

    });

    const Heading_tag = `${attr.attributes.titleTag}`;

    return ( 

        <>

            { attr.innerBlocks && 
            
                <div className={ `${ columns ?  classes : 'col-12' }` }>                
    
                    <div className={`accordion-item ${styles['accordion-item']}  ${styles['breakpoint-' + attr.breakpoint]} ${styles[display]} ${ ( display == 'with-images' ) ? styles['with-images'] : '' }  ${ ( attr.addNumbers ) ? styles.add_numbers : '' } `}>
                                               
                        { accordian < breakpoint && 

                            <>

                            <Heading_tag className={`accordion-header ${attr?.attributes?.titleStyle} ${styles['accordion-header']}` } id={heading_id}>

                                <button className={`accordion-button ${styles['accordion-button']}  ${ i == 0 ? 'show' : 'collapsed' }` } type="button" data-bs-toggle="collapse" data-bs-target={ '#' + collapse_id } aria-expanded={ i == 0 ? 'true' : 'false' } aria-controls={collapse_id}>

                                    { attr.addNumbers == true  && <span className={styles.number}>{ ('0' + numbers).slice(-2) } </span> }  { parseHtml( title ) }

                                </button>

                            </Heading_tag>


                            {   display == 'with-images' && 

                                <div className={`accordion-additonal d-none`} id={ "additonal_" + collapse_id }>

                                    <div className={`row ${styles['accordian_image_wrapper']} ${ iteration ? styles.even : styles.odd } ` }>

                                        <div className={styles.accordian_images}>

                                            { mediaURL1 != null && 
                                                
                                                <img
                                                    src={ mediaURL1 }
                                                    layout='fill'
                                                    className={`accordian-image-1`}
                                                />

                                            } 
                                        
                                        </div>
                                        <div className={styles.accordian_images}>
                                            

                                            { mediaURL2 != null && 
                                                
                                                <img
                                                    src={ mediaURL2 }
                                                    layout='fill'
                                                    className={`accordian-image-2`}
                                                />

                                            } 
                                            
                                        </div>

                                    </div>

                                </div>

                            }
                            
                            <div id={collapse_id} className={`accordion-collapse collapse  ${ i == 0 ? 'show' : 'collapsed' }` } aria-labelledby={heading_id} data-bs-parent={ '#' + attr.parent }>

                                                      
                                <div className={`accordion-body ${styles['accordion-body']}` }>

                                    { attr.innerBlocks.map( ( block, index ) => {

                                        return (

                                            <React.Fragment key={index} >

                                                {Components( block, index ) }

                                            </React.Fragment>

                                        );

                                    } ) }

                                </div>
                                
                            </div>

                            </>

                        }

                        { accordian >= breakpoint && 

                            <>

                                <h2 className={`accordion-header ${styles['accordion-header']}` } id={heading_id}> {title}</h2>

                                <div className="accordion-collapse" aria-labelledby={heading_id} data-bs-parent={ '#' + attr.parent }>

                                                    
                                    <div className={`accordion-body ${styles['accordion-body']}` }>

                                        { attr.innerBlocks.map( ( block, index ) => {

                                            return (

                                                <React.Fragment key={index} >

                                                    {Components( block, index ) }

                                                </React.Fragment>

                                            );

                                        } ) }

                                    </div>

                                </div>

                            </>

                        }

                    </div>

                </div>

            }

        </>

    )

}