import parseHtml from "../utils/parser";

export default function CoreParagraphBlock( block ) {

    const attr = block.block;
    const content  = parseHtml( attr.attributes.content, attr.attributes.className )
    const className = attr.attributes.className
    
    return ( 

        <>
            <p className={ className }> {  content  } </p> 

        </>

    )

}
  