import parseHtml from "../utils/parser";

export default function CoreHeadingBlock( block ) {

    const attr = block.block
    const level = attr?.attributes?.level ?  attr.attributes.level : 3;
    const Heading_tag = `h${level}`;


    const title =attr?.attributes?.content? parseHtml( attr.attributes.content ) : undefined;

    return ( 
    
        <>

            <Heading_tag className={ `${ attr?.attributes?.textColor?  'text-' + attr.attributes.textColor : '' }${attr.attributes?.className}` } >{ title }</Heading_tag> 
    
        </>
    
    )

}
  