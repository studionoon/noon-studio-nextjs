import cn from 'classnames'
import Image from 'next/image'
import Link from 'next/link'
import React, { useState, useEffect } from 'react';

const NoonButton = React.forwardRef(({ onClick, href, image, title }, ref) => {
    return (
        <a href={href} aria-label={title}>
            {image}
        </a>
    )
})

export default function CoverImage({ title, coverImage, slug }) {

    const image = (

        <Image
            width={2000}
            height={1000}
            alt={`Cover Image for ${title}`}
            src={coverImage?.sourceUrl}
            className={cn('shadow-small', {
                'hover:shadow-medium transition-shadow duration-200': slug,
            })}
        />

    )
    return (

        <div className="sm:mx-0">
            {slug ? (
                <Link href={`/posts/${slug}`} passHref legacyBehavior>
                    <NoonButton />
                </Link>
            ) : (
                image
            )}
        </div>

    )

}
