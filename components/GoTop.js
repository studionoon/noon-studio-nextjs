import { useEffect, useState } from "react";
import styles from './GoTop.module.scss'

const GoTop = (props) => {

    const [showButton, setShowButton] = useState(false);

    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.pageYOffset > 300) {
                setShowButton(true);
            } else {
                setShowButton(false);
            }
        });
    }, []);
  
    // This function will scroll the window to the top 
    const scrollToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth' // for smoothly scrolling
        });
    };

    return (

      <>

        {showButton && (

            <button onClick={scrollToTop} className={`backToTop ${styles.back_to_top} `}>
                
                <i className="goTop__text fas fa-chevron-up" />

            </button>

        )}

      </>

    );

};

export default GoTop;