import Head from 'next/head'
import Favicon from './favicon'

export default function Meta( { page }) {

const seo = page?.seo

    return (
        <Head>

            <Favicon />

            {/*  YOAST SEO */}

            {/* Primary Meta Tags */}

            { seo?.title && 
                <title>{seo.title}</title>
            }

            { seo?.title && 
                <meta name="title" content={seo.title} />
            } 
            
            { seo?.metaDesc && 
                <meta name="description" content={seo.metaDesc} />
            } 

            {/* Open Graph / Facebook */}
                {/* og:type */}
                { seo?.twitterImage && 
                    <meta property="og:type" content="website" />
                } 

                {/* og:url */}
                { seo?.opengraphUrl && 
                    <meta property="og:url" content={ seo?.opengraphUrl} />
                } 

                {/* og:title */}
                { seo?.opengraphTitle && 
                    <meta property="og:title" content={ seo?.opengraphTitle} />
                } 

                {/* og:description */}
                { seo?.opengraphDescription && 
                    <meta property="og:description" content={ seo?.opengraphDescription} />
                } 

                {/* og:image */}
                { seo?.opengraphImage && 
                    <meta property="og:image" content={ seo?.opengraphImage.sourceUrl} />
                } 


            {/* Twitter */}
                {/* twitter:card */}
                { seo?.twitterImage && 
                    <meta property="twitter:card" content="summary_large_image"/>
                } 

                {/* twitter:url */}
                { seo?.twitterImage && 
                    <meta property="twitter:image" content={ seo?.twitterImage} />
                } 

                {/* twitter:title */}
                { seo?.twitterTitle && 
                    <meta property="twitter:title" content={ seo?.twitterTitle} />
                } 

                {/* twitter:description */}
                { seo?.twitterDescription && 
                    <meta property="twitter:description" content={ seo?.twitterDescription} />
                } 

                {/* twitter:image */}
                { seo?.twitterImage && 
                    <meta property="twitter:image" content={ seo?.twitterImage} />
                } 
            
        </Head>

    )

}
