import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState, useEffect, forwardRef } from "react";

import styles from "./navigation.module.scss";
import parsePhoneNumber from "libphonenumber-js";

const NoonButtonHome = forwardRef(({ onClick, href, page }, ref) => {
  return (
    <a href={href}>
      <svg
        aria-label="Studio Noon"
        xmlns="http://www.w3.org/2000/svg"
        width="38.55mm"
        height="54"
        viewBox="0 0 109.29 24.67"
        className={`noon_logo ${styles.noon_logo} ${
          styles[page?.pageSettings?.initialNavagation]
        }`}
      >
        <g>
          <path d="M0,.53H5.76V3.07l.09.05A11.68,11.68,0,0,1,13.63,0a8.26,8.26,0,0,1,6.24,2.5c1.39,1.49,2,3.46,2,7V24.15H16.08V10.46c0-2.11-.29-3.26-1-4A3.84,3.84,0,0,0,12,5.14a9,9,0,0,0-6.19,3v16H0Z" />
          <path d="M39.9,0c7.34,0,12.82,5.38,12.82,12.34S47.24,24.67,39.9,24.67,27.09,19.3,27.09,12.34,32.56,0,39.9,0Zm0,19.59a7,7,0,0,0,7-7.25,7,7,0,1,0-14,0A7,7,0,0,0,39.9,19.59Z" />
          <path d="M69.29,0C76.63,0,82.1,5.38,82.1,12.34S76.63,24.67,69.29,24.67,56.48,19.3,56.48,12.34,62,0,69.29,0Zm0,19.59a7,7,0,0,0,7-7.25,7,7,0,1,0-14,0A7,7,0,0,0,69.29,19.59Z" />
          <path d="M87.4.53h5.76V3.07l.09.05A11.68,11.68,0,0,1,101,0a8.24,8.24,0,0,1,6.24,2.5c1.39,1.49,2,3.46,2,7V24.15h-5.81V10.46c0-2.11-.29-3.26-1-4a3.84,3.84,0,0,0-3.12-1.3,9,9,0,0,0-6.19,3v16H87.4Z" />
        </g>
      </svg>
    </a>
  );
});

const NoonButtonMenu = forwardRef(
  ({ onClick, href, innerText, className }, ref) => {
    return (
      <a
        href={href}
        className={"menu__link " + className}
        dangerouslySetInnerHTML={{ __html: innerText }}
      />
    );
  }
);

const Navigation = ({ page, contact, menuItems: { menuItems } }) => {
  const router = useRouter();

  if (router.asPath.length != 1) {
    router.asPath += "/";
  }

  const [value, setValue] = useState(false);

  const clicked = (e) => {
    setValue(!value);
  };

  useEffect(() => {
    if (value) {
      document.querySelector("body").classList.add("nav_is_open");

      window.onkeydown = function (event) {
        if (event.code == "BracketRight" || event.keyCode == 221) {
          setValue(false);
        }
      };
    } else {
      document.querySelector("body").classList.remove("nav_is_open");

      window.onkeydown = function (event) {
        if (event.code == "BracketLeft" || event.keyCode == 219) {
          setValue(true);
        }
      };
    }
  }, [value]);

  // Variables
  const email_default =
    contact?.acfOptionsContact?.contactSettings?.emailDefault;
  const phone_default = contact?.acfOptionsContact?.contactSettings?.phone;
  const phoneNumber = parsePhoneNumber(phone_default, "GB");

  const [scroll, setScroll] = useState(0);
  const [nav_scrolled, setNavScrolled] = useState(false);

  // Scroll
  useEffect(() => {
    const scrollHandler = () => {
      let nav_scrolled = false;
      setScroll(window.pageYOffset);
      // If scroll is over 100 lets hide the logo and then move the toggle
      if (window.pageYOffset > 100 && nav_scrolled == false) {
        setNavScrolled(true);
      } else {
        setNavScrolled(false);
      }
    };

    window.addEventListener("scroll", scrollHandler);
  });

  return (
    <>
      <div className={"menu"}>
        <div id="nav_main__content" className={"menu__content"}>
          <div className={"container-standard container"}>
            <div className={"row h-noon-menu"}>
              <div className={"col-12 my-auto"}>
                <div className={"grid"}>
                  <nav>
                    <ul className={"menu__primary"}>
                      <li>
                        <Link href="/" passHref legacyBehavior>
                          <NoonButtonMenu innerText="Home" />
                        </Link>
                      </li>
                      <li>
                        <Link href="/about" passHref legacyBehavior>
                          <NoonButtonMenu innerText="About" />
                        </Link>
                      </li>
                      <li className="nav-disabled">
                        <Link href="" legacyBehavior>
                          <NoonButtonMenu innerText="Work <span class='coming-soon'>Coming Soon</span>" />
                        </Link>
                      </li>
                      <li>
                        <Link href="/our-approach" passHref legacyBehavior>
                          <NoonButtonMenu innerText="Our Approach" />
                        </Link>
                      </li>
                      <li>
                        <Link href="/our-services" passHref legacyBehavior>
                          <NoonButtonMenu innerText="Our Services" />
                        </Link>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>

          <div className={`contact_banner `}>
            <div className={`container`}>
              <div className={`row`}>
                {email_default.length > 0 && (
                  <>
                    <div className={`col-12 col-md-6 col-lg-3 col_email`}>
                      <div className={"col_email_wrapper"}>
                        <h4>Email</h4>
                        <a
                          className={"col_email__link"}
                          href={"mailto:" + email_default}
                        >
                          {email_default}
                        </a>
                      </div>
                    </div>
                  </>
                )}

                {phone_default.length > 0 && (
                  <>
                    <div className={`col-12 col-md-6 col-lg-3 col_phone`}>
                      <div className={"col_phone_wrapper"}>
                        <h4>Phone</h4>
                        <a
                          className={"col_phone__link"}
                          href={"tel:" + phoneNumber.number}
                        >
                          {phoneNumber.formatNational()}
                        </a>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>

      <nav
        className={`navbar  bg-light fixed-top  ${styles.navbar}   ${
          nav_scrolled ? styles["nav_scrolled"] : ""
        }`}
      >
        <div
          className={`container ${styles.containerTop} ${styles.pagenavbartop} `}
        >
          <div className={`row my-auto ${styles.rowTop}`}>
            <div className={`col ${styles.width100}`}>
              <Link href="/" passHref legacyBehavior>
                <NoonButtonHome />
              </Link>

              <button
                onClick={clicked}
                id="nav_button"
                className={"menu-trigger js-menu-trigger text-replace"}
              >
                <span
                  className={"menu-trigger__icon"}
                  aria-hidden="true"
                ></span>

                <svg viewBox="0 0 54 54" aria-hidden="true">
                  <circle
                    fill="transparent"
                    strokeWidth="1"
                    cx="27"
                    cy="27"
                    r="25"
                    strokeDasharray="157 157"
                    strokeDashoffset="157"
                  ></circle>
                </svg>
              </button>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
};
export default Navigation;
