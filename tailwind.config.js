/** @type {import('tailwindcss').Config} */

const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  mode: "jit",
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./components/**/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ["niveau-grotesk", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        accent: {
          DEFAULT: "#ed5c5a",
          100: "#ed5c5a",
          80: "#f17c7a",
          7: "#fef4f4",
        },
        primary: {
          DEFAULT: "#02182b",
          100: "#02182b",
          95: "#0e2335",
          65: "#5a6875",
          5: "#eeeff1",
        },
        white: {
          DEFAULT: "#fdfffc",
          100: "#fdfffc",
          70: "#fdfffc",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
