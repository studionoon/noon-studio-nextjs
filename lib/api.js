const API_URL = process.env.WORDPRESS_API_URL

async function fetchAPI(query, { variables } = {}) {
  const headers = { 'Content-Type': 'application/json' }

  if (process.env.WORDPRESS_AUTH_REFRESH_TOKEN) {
    headers[
      'Authorization'
    ] = `Bearer ${process.env.WORDPRESS_AUTH_REFRESH_TOKEN}`
  }

  const res = await fetch(API_URL, {
    method: 'POST',
    headers,
    body: JSON.stringify({
      query,
      variables,
    }),
  })

  const json = await res.json()
  if (json.errors) {
    console.error(json.errors, json)
    throw new Error('Failed to fetch API')
  }
  return json.data
}

export async function getPreviewPost(id, idType = 'DATABASE_ID') {
  const data = await fetchAPI(
    `
    query PreviewPost($id: ID!, $idType: PostIdType!) {
      post(id: $id, idType: $idType) {
        databaseId
        slug
        status
      }
    }`,
    {
      variables: { id, idType },
    }
  )
  return data.post
}

export async function getAllPostsWithSlug() {
  const data = await fetchAPI(`
    {
      posts(first: 10000) {
        edges {
          node {
            slug
          }
        }
      }
    }
  `)
  return data?.posts
}

export async function getAllPostsForHome(preview) {
  const data = await fetchAPI(
    `
    query AllPosts {
      posts(first: 20, where: { orderby: { field: DATE, order: DESC } }) {
        edges {
          node {
            title
            excerpt
            slug
            date
            featuredImage {
              node {
                sourceUrl
              }
            }
            author {
              node {
                name
                firstName
                lastName
                avatar {
                  url
                }
              }
            }
          }
        }
      }
    }
  `,
    {
      variables: {
        onlyEnabled: !preview,
        preview,
      },
    }
  )

  return data?.posts
}

export async function getPrimaryMenu() {
  const data = await fetchAPI(`
  query GETPRIMARUMENU {
    menus(where: {location: PRIMARY}) {
      nodes {
        menuItems {
          edges {
            node {
              path
              label
              menuSettings {
                itemSize
              }
            }
          }
        }
      }
    }
  }
  `);
  return data?.menus?.nodes[0];
}
export async function getFooter1Menu() {
  const data = await fetchAPI(`
  query GETFOOTER1MENU {
    menus(where: {location: FOOTER_1}) {
      nodes {
        menuItems {
          edges {
            node {
              path
              label
            }
          }
        }
      }
    }
  }
  `);
  return data?.menus?.nodes[0];
}
export async function getFooter2Menu() {
  const data = await fetchAPI(`
  query GETFOOTER2MENU {
    menus(where: {location: FOOTER_2}) {
      nodes {
        menuItems {
          edges {
            node {
              path
              label
            }
          }
        }
        name
      }
    }
  }
  `);
  return data?.menus?.nodes[0];
}



export async function getFooterMenus() {

  const data = [ 'footer_1', 'footer_2', 'primary' ]

  const primary = await fetchAPI(`
  query GETPRIMARUMENU {
    menus(where: {location: PRIMARY}) {
        nodes {
            menuItems {
                edges {
                    node {
                        path
                        label
                    }
                }
            }
        }
    }
  }
  `);

  const footer_1 = await fetchAPI(`
  query GETFOOTER_1 {
      menus(where: {location: FOOTER_1}) {
          nodes {
              menuItems {
                  edges {
                      node {
                          path
                          label
                      }
                  }
              }
          }
      }
    }
  `);

  const footer_2 = await fetchAPI(`
   query GETFOOTER_2 {
      menus(where: {location: FOOTER_2}) {
          nodes {
              menuItems {
                  edges {
                      node {
                          path
                          label
                      }
                  }
              }
          }
      }
    }      
  `);    


  if( footer_1?.menus?.nodes[0] ){

    data.footer_1 = footer_1?.menus?.nodes[0]

  } 

  // if( footer_2?.menus?.nodes[0] ){

  //   data.footer_2 = footer_2?.menus?.nodes[0]

  // } 

  return data;
}


export async function getContactDetails() {
  const data = await fetchAPI(`
  query CONTACT_DETAILS {
    acfOptionsContact {
      contactSettings {
        address
        addressAlternative
        emailDefault
        emailJobs
        phone
      }
    }
    allSettings {
      generalSettingsTitle
    }
  }
  `);
  return data;
}



export async function getPostAndMorePosts(slug, preview, previewData) {
  const postPreview = preview && previewData?.post
  // The slug may be the id of an unpublished post
  const isId = Number.isInteger(Number(slug))
  const isSamePost = isId
    ? Number(slug) === postPreview.id
    : slug === postPreview.slug
  const isDraft = isSamePost && postPreview?.status === 'draft'
  const isRevision = isSamePost && postPreview?.status === 'publish'
  const data = await fetchAPI(
    `
    fragment AuthorFields on User {
      name
      firstName
      lastName
      avatar {
        url
      }
    }
    fragment PostFields on Post {
      title
      excerpt
      slug
      date
      featuredImage {
        node {
          sourceUrl
        }
      }
      author {
        node {
          ...AuthorFields
        }
      }
      categories {
        edges {
          node {
            name
          }
        }
      }
      tags {
        edges {
          node {
            name
          }
        }
      }
    }
    query PostBySlug($id: ID!, $idType: PostIdType!) {
      post(id: $id, idType: $idType) {
        ...PostFields
        content
        ${
          // Only some of the fields of a revision are considered as there are some inconsistencies
          isRevision
            ? `
        revisions(first: 1, where: { orderby: { field: MODIFIED, order: DESC } }) {
          edges {
            node {
              title
              excerpt
              content
              author {
                node {
                  ...AuthorFields
                }
              }
            }
          }
        }
        `
            : ''
        }
      }
      posts(first: 3, where: { orderby: { field: DATE, order: DESC } }) {
        edges {
          node {
            ...PostFields
          }
        }
      }
    }
  `,
    {
      variables: {
        id: isDraft ? postPreview.id : slug,
        idType: isDraft ? 'DATABASE_ID' : 'SLUG',
      },
    }
  )

  // Draft posts may not have an slug
  if (isDraft) data.post.slug = postPreview.id
  // Apply a revision (changes in a published post)
  if (isRevision && data.post.revisions) {
    const revision = data.post.revisions.edges[0]?.node

    if (revision) Object.assign(data.post, revision)
    delete data.post.revisions
  }

  // Filter out the main post
  data.posts.edges = data.posts.edges.filter(({ node }) => node.slug !== slug)
  // If there are still 3 posts, remove the last one
  if (data.posts.edges.length > 2) data.posts.edges.pop()

  return data
}

export async function getAllPagesWithSlugs() {
  const data = await fetchAPI(`
  {
    pages(first: 10000) {
      edges {
        node {
          slug
        }
      }
    }
  }
  `);
  return data?.pages;
}

export async function getAllWorkWithSlugs() {
  const data = await fetchAPI(`
  {
    works(first: 10000) {
      edges {
        node {
          slug
        }
      }
    }
  }
  `);
  return data?.works;
}



export async function getPageBySlug(slug) {
  const data = await fetchAPI(`

    fragment ParagraphFields on CoreParagraphBlock {
      __typename
      attributes {
        ... on CoreParagraphBlockAttributes {
          align
          content
          className
        }
      }
    }

    fragment HeadingFields on CoreHeadingBlock {
      __typename
      attributes {
        ... on CoreHeadingBlockAttributes {
          level
          content
          textColor
          className
        }
      }
    }    

    fragment CoreGalleryFields on CoreGalleryBlock {
      __typename
      attributes {
        ... on CoreGalleryBlockAttributes {
            align
            anchor
            images {
                alt
                caption
                id
                url
            }
            className
            columns
            ids
            imageCrop
            caption
        }
      }
    }    

    fragment ListFields on CoreListBlock {
      __typename
      attributes {
        ... on CoreListBlockAttributes{
          className
          addListCheckMark
          values
          showColumnsOnMobile
          listColumns     
          hideTickIcon
        }
      }
      innerBlocks{
        ... on CoreListItemBlock {
          __typename
          attributes {
            content
          }
        }
      }
    }  

    fragment ImageFields on CoreImageBlock {
      __typename
      attributes {
        ... on CoreImageBlockAttributes {
          id
          height
          width
          url
          sizeSlug
          align
          useImageSize
        }
      }
    }    
  
    fragment CarouselImageFields on NoonCarouselImageBlock {
      __typename
      attributes {
        mediaURL
        focalPoint
      }
    }

    fragment CoreMediaTextFields on CoreMediaTextBlock {
      __typename
      innerBlocks{
        __typename

        ... on CoreParagraphBlock {
          ...ParagraphFields
        }
        ... on CoreButtonsBlock {
          ...ButtonsFields
        }
        ... on CoreHeadingBlock {
          ...HeadingFields
        }
        ... on CoreListBlock {
          ...ListFields
        }
        ... on CoreColumnsBlock {
            ...CoreColumnsFields
        }
        ... on NoonAccordianParentBlock {
            ...NoonAccordianFields
        }
        ... on CoreSpacerBlock {
          ...CoreSpacerFields
        } 

      }
      attributes {
        ... on CoreMediaTextBlockAttributes {
          className
          focalPoint
          mediaUrl
          mediaWidth
          mediaPosition
          imageBreakpoints
          imageBreakpointUnits
          uniqueID
          addBg
          verticalAlignment
          backgroundColor
          textColor
          halfPage
        }
      }
    } 
    
    fragment HeaderImageFields on NoonHeaderImageBlock {
      __typename
        attributes {
          className
          focalPoint
          headerimageHeading
          headerimageTitle
          mediaID
          mediaURL
          backgroundColor
          textColor
        }
        innerBlocks{
            __typename
  
            ... on CoreParagraphBlock {
              ...ParagraphFields
            }
  
            ... on CoreHeadingBlock {
              ...HeadingFields
            }
  
            ... on CoreListBlock {
              ...ListFields
            }
        }
    } 

    fragment CoverImageFields on NoonCoverImageBlock {
      __typename
      attributes {
        focalPoint
        mediaURL
        mediaID
        className
      }
    }    

    fragment ButtonsFields on CoreButtonsBlock {
      __typename
      name
      attributes {
        ... on CoreButtonsBlockAttributes {
          align
        }
      }
      innerBlocks {
          __typename
          ... on CoreButtonBlock {
            attributes {
              ... on CoreButtonBlockAttributes {
                backgroundColor
                linkTarget
                text
                title
                url
                className
              }
            }
            name
          }
        }

    }

    fragment CarouselFields on NoonCarouselBlock {

      innerBlocks {
        __typename
        ... on NoonCarouselChildBlock {
          __typename
          innerBlocks{
            __typename
            ... on NoonCarouselImageBlock {
              ...CarouselImageFields
            }
            ... on CoreParagraphBlock {
              ...ParagraphFields
            }
            ... on CoreHeadingBlock {
              ...HeadingFields
            }
            ... on CoreListBlock {
              ...ListFields
            }
            ... on CoreImageBlock {
              ...ImageFields
            }
          }
        }
      }
      attributes {
        className
        groupCTA
        groupIntro
        groupTitle
        breakpoints
        imageBreakpoints
        imageBreakpointUnits
        backgroundColor
        textColor
        uniqueID
      }
    }

    fragment NoonColumnheaderFields on NoonColumnheaderBlock {
        __typename
        attributes {
            addIntro
            addTitle
            className
            groupIntro
            groupTitle
            titleSize
        }
    }

    fragment CoreSpacerFields on CoreSpacerBlock{
      __typename
      attributesJSON
      attributes{
        ... on CoreSpacerBlockAttributes {

          heightXS
          heightSM
          heightMD
          heightLG
          heightXL
          heightXXL
        }
      }
    }


    fragment CoreColumnsFields on CoreColumnsBlock {
      innerBlocks {
        __typename

        ... on CoreColumnBlock {
          innerBlocks {
            __typename

            ... on CoreSpacerBlock {
                ...CoreSpacerFields
            }

            ... on NoonAccordianParentBlock {
              ...NoonAccordianFields
            }
            ... on CoreParagraphBlock {
              ...ParagraphFields
            }
            ... on CoreHeadingBlock {
              ...HeadingFields
            }
            ... on CoreListBlock {
              ...ListFields
            }
            ... on CoreImageBlock {
                ...ImageFields
            }
            ... on NoonColumnheaderBlock {
                ...NoonColumnheaderFields
            }
          }
          attributes {
            ... on CoreColumnBlockAttributes {
              className
            }
          }
        }
      }
      attributes {
        ... on CoreColumnsBlockAttributes {
          className
        }
      }
    }

    fragment NoonAccordianFields on NoonAccordianParentBlock {
      innerBlocks {
        __typename
        ... on NoonAccordianChildBlock {
          __typename
          innerBlocks {
            __typename
            ... on CoreParagraphBlock {
              ...ParagraphFields
            }
            ... on CoreHeadingBlock {
              ...HeadingFields
            }
            ... on CoreListBlock {
              ...ListFields
            }
          }
          attributes {
            acccordianHeader
            className
            columnsXXL
            columnsXS
            columnsXL
            columnsSM
            columnsMD
            columnsLG
            mediaURL2
            mediaURL1
            titleTag
            titleStyle
          }
        }
      }
      attributes {
        className
        accordianDisplay
        accordion
        defaultOpen
        colorName
        textColorName
        uniqueID
        addNumbers
      }
    }
    

    
    query{

    page(id: "${slug}", idType: URI) {
      title

      seo {
        canonical
        title
        metaDesc
        focuskw
        metaRobotsNoindex
        metaRobotsNofollow
        opengraphAuthor
        opengraphDescription
        opengraphTitle
        opengraphDescription
        opengraphImage {
            sourceUrl(size: OG_IMAGE)
        }
        opengraphUrl
        opengraphSiteName
        opengraphPublishedTime
        opengraphModifiedTime
        twitterTitle
        twitterDescription
        twitterImage {
            sourceUrl(size: OG_IMAGE)
        }
        breadcrumbs {
            url
            text
        }
        cornerstone
        schema {
            pageType
            articleType
            raw
        }
        readingTime
        fullHead
      }
      pageSettings {
        initialNavagation
      }
      blocks {
        __typename
        name

        ... on CoreGalleryBlock{
            ...CoreGalleryFields
        }
        ... on NoonHeaderImageBlock{
            ...HeaderImageFields
         }

        ... on CoreMediaTextBlock {
          ...CoreMediaTextFields
        }
        ... on NoonCarouselBlock {
          ...CarouselFields
        }
        ... on CoreButtonsBlock {
          ...ButtonsFields
        }
        ...on CoreImageBlock {
          ...ImageFields
        }
        ... on NoonCoverImageBlock {
          ...CoverImageFields
        }
        ... on CoreParagraphBlock {
          ...ParagraphFields
        }
        ... on CoreListBlock {
          ...ListFields
        }
        ... on NoonColumnheaderBlock {
          ...NoonColumnheaderFields
        }

        ... on NoonGroupBlock {
          attributes {
            backgroundColor
            className
            groupIntro
            groupTitle
            textColor
            addTitle
            addIntro
            titleSize
            groupPadding
            groupSize
          }
          innerBlocks {
            __typename
            
            ... on NoonAccordianParentBlock {
              ...NoonAccordianFields
            }

            ... on CoreGalleryBlock {
                ...CoreGalleryFields
            }
            ... on CoreMediaTextBlock {
              ...CoreMediaTextFields
            }

            ... on CoreColumnsBlock {
              ...CoreColumnsFields
            }

            ... on CoreSpacerBlock {
              ...CoreSpacerFields
            } 

            ... on NoonCarouselBlock{
              ...CarouselFields
            }


          }
        }
        ... on CoreGroupBlock {
          attributes {
            ... on CoreGroupBlockAttributes {
              backgroundColor
              textColor
            }
          }
          isDynamic
          innerBlocks {
            __typename
            ... on CoreColumnsBlock {
              innerBlocks {
                __typename
                ... on CoreColumnBlock {
                  innerBlocks {
                    __typename
                    ... on CoreParagraphBlock {
                      ...ParagraphFields
                    }
                    ... on CoreHeadingBlock {
                      ...HeadingFields
                    }
                    ... on CoreListBlock {
                      ...ListFields
                    }
                  }
                  attributes {
                    ... on CoreColumnBlockAttributes {
                      className
                    }
                  }
                }
              }
              attributes {
                ... on CoreColumnsBlockAttributes {
                  className
                }
              }
            }
          }
        }
        ... on NoonAccordianParentBlock {
            ...NoonAccordianFields
        }
      }
    }
  }

  `);
  return data?.page;
}


export async function getWorkBySlug(slug) {
  const data = await fetchAPI(`
    fragment ParagraphFields on CoreParagraphBlock {
      __typename
      attributes {
        ... on CoreParagraphBlockAttributes {
          align
          content
          className
        }
      }
    }

    fragment HeadingFields on CoreHeadingBlock {
      __typename
      attributes {
        ... on CoreHeadingBlockAttributes {
          level
          content
          textColor
          className
        }
      }
    }    

    fragment CoreGalleryFields on CoreGalleryBlock {
      __typename
      attributes {
        ... on CoreGalleryBlockAttributes {
            align
            anchor
            images {
                alt
                caption
                id
                url
            }
            className
            columns
            ids
            imageCrop
            caption
        }
      }
    }    

    fragment ListFields on CoreListBlock {
      __typename
      attributes {

        ... on CoreListBlockAttributes{
          className
          addListCheckMark
          values
          showColumnsOnMobile
          listColumns     

        }
      }
    }  
    
    fragment ImageFields on CoreImageBlock {
      __typename
      attributes {
        ... on CoreImageBlockAttributes {
          id
          height
          width
          url
          sizeSlug
          align
          useImageSize
        }
      }
    }    
  
    fragment CarouselImageFields on NoonCarouselImageBlock {
      __typename
      attributes {
        mediaURL
        focalPoint
      }
    }

    fragment CoreMediaTextFields on CoreMediaTextBlock {
      __typename
      innerBlocks{
        __typename

        ... on CoreParagraphBlock {
          ...ParagraphFields
        }
        ... on CoreButtonsBlock {
          ...ButtonsFields
        }
        ... on CoreHeadingBlock {
          ...HeadingFields
        }
        ... on CoreListBlock {
          ...ListFields
        }


      }
      attributes {
        ... on CoreMediaTextBlockAttributes {
          className
          focalPoint
          mediaUrl
          mediaWidth
          mediaPosition
          imageBreakpoints
          imageBreakpointUnits
          uniqueID
          addBg
          backgroundColor
          textColor
          halfPage
        }
      }
    } 
    
    fragment HeaderImageFields on NoonHeaderImageBlock {
      __typename
        attributes {
          className
          focalPoint
          headerimageHeading
          headerimageTitle
          mediaID
          mediaURL
          backgroundColor
          textColor
        }
        innerBlocks{
            __typename
  
            ... on CoreParagraphBlock {
              ...ParagraphFields
            }
  
            ... on CoreHeadingBlock {
              ...HeadingFields
            }
  
            ... on CoreListBlock {
              ...ListFields
            }
        }
    } 

    fragment CoverImageFields on NoonCoverImageBlock {
      __typename
      attributes {
        focalPoint
        mediaURL
        mediaID
        className
      }
    }    

    fragment ButtonsFields on CoreButtonsBlock {
      __typename
      name
      attributes {
        ... on CoreButtonsBlockAttributes {
          align
        }
      }
      innerBlocks {
          __typename
          ... on CoreButtonBlock {
            attributes {
              ... on CoreButtonBlockAttributes {
                backgroundColor
                linkTarget
                text
                title
                url
                className
              }
            }
            name
          }
        }

    }

    fragment CarouselFields on NoonCarouselBlock {

      innerBlocks {
        __typename
        ... on NoonCarouselChildBlock {
          __typename
          innerBlocks{
            __typename
            ... on NoonCarouselImageBlock {
              ...CarouselImageFields
            }
            ... on CoreParagraphBlock {
              ...ParagraphFields
            }
            ... on CoreHeadingBlock {
              ...HeadingFields
            }
            ... on CoreListBlock {
              ...ListFields
            }
            ... on CoreImageBlock {
              ...ImageFields
            }
          }
        }
      }
      attributes {
        className
        groupCTA
        groupIntro
        groupTitle
        breakpoints
        imageBreakpoints
        imageBreakpointUnits
        backgroundColor
        textColor
        uniqueID
      }
    }

    fragment CoreColumnsFields on CoreColumnsBlock {
      innerBlocks {
        __typename

        ... on CoreColumnBlock {
          innerBlocks {
            __typename

            ... on NoonAccordianParentBlock {
              ...NoonAccordianFields
            }
            ... on CoreParagraphBlock {
              ...ParagraphFields
            }
            ... on CoreHeadingBlock {
              ...HeadingFields
            }
            ... on CoreListBlock {
              ...ListFields
            }

            ... on CoreImageBlock {
                ...ImageFields
            }

          }
          attributes {
            ... on CoreColumnBlockAttributes {
              className
            }
          }
        }
      }
      attributes {
        ... on CoreColumnsBlockAttributes {
          className
        }
      }
    }

    fragment NoonAccordianFields on NoonAccordianParentBlock {
      innerBlocks {
        __typename
        ... on NoonAccordianChildBlock {
          __typename
          innerBlocks {
            __typename
            ... on CoreParagraphBlock {
              ...ParagraphFields
            }
            ... on CoreHeadingBlock {
              ...HeadingFields
            }
            ... on CoreListBlock {
              ...ListFields
            }
          }
          attributes {
            acccordianHeader
            className
            columnsXXL
            columnsXS
            columnsXL
            columnsSM
            columnsMD
            columnsLG
            mediaURL2
            mediaURL1
            titleTag
            titleStyle

          }
        }
      }
      attributes {
        className
        accordianDisplay
        accordion
        defaultOpen
        colorName
        textColorName
        uniqueID
      }
    }
    

    
    query{

    work(id: "${slug}", idType: URI) {
      title

      seo {
        canonical
        title
        metaDesc
        focuskw
        metaRobotsNoindex
        metaRobotsNofollow
        opengraphAuthor
        opengraphDescription
        opengraphTitle
        opengraphDescription
        opengraphImage {
            sourceUrl(size: OG_IMAGE)
        }
        opengraphUrl
        opengraphSiteName
        opengraphPublishedTime
        opengraphModifiedTime
        twitterTitle
        twitterDescription
        twitterImage {
            sourceUrl(size: OG_IMAGE)
        }
        breadcrumbs {
            url
            text
        }
        cornerstone
        schema {
            pageType
            articleType
            raw
        }
        readingTime
        fullHead
      }

      blocks {
        __typename
        name

        ... on CoreGalleryBlock{
            ...CoreGalleryFields
        }

        ... on NoonHeaderImageBlock{
            ...HeaderImageFields
         }
 
        ... on CoreMediaTextBlock {
          ...CoreMediaTextFields
        }

        ... on NoonCarouselBlock {
          ...CarouselFields
        }
        ... on CoreButtonsBlock {
          ...ButtonsFields
        }
        ...on CoreImageBlock {
          ...ImageFields
        }
        ... on NoonCoverImageBlock {
          ...CoverImageFields
        }
        ... on CoreParagraphBlock {
          ...ParagraphFields
        }
        ... on CoreListBlock {
          ...ListFields
        }
        ... on NoonGroupBlock {
          attributes {
            backgroundColor
            className
            groupIntro
            groupTitle
            textColor
            addTitle
            addIntro
            titleSize
            groupPadding
            groupSize
          }
          innerBlocks {
            __typename
            
            ... on NoonAccordianParentBlock {
              ...NoonAccordianFields
            }

            ... on CoreGalleryBlock {
                ...CoreGalleryFields
            }
            ... on CoreMediaTextBlock {
              ...CoreMediaTextFields
            }

            ... on CoreColumnsBlock {
              ...CoreColumnsFields
            }

            ... on CoreSpacerBlock {
              ...CoreSpacerFields
            } 

          }
        }
        ... on CoreGroupBlock {
          attributes {
            ... on CoreGroupBlockAttributes {
              backgroundColor
              textColor
            }
          }
          isDynamic
          innerBlocks {
            __typename
            ... on CoreColumnsBlock {
              innerBlocks {
                __typename
                ... on CoreColumnBlock {
                  innerBlocks {
                    __typename
                    ... on CoreParagraphBlock {
                      ...ParagraphFields
                    }
                    ... on CoreHeadingBlock {
                      ...HeadingFields
                    }
                    ... on CoreListBlock {
                      ...ListFields
                    }
                  }
                  attributes {
                    ... on CoreColumnBlockAttributes {
                      className
                    }
                  }
                }
              }
              attributes {
                ... on CoreColumnsBlockAttributes {
                  className
                }
              }
            }
          }
        }
        ... on NoonAccordianParentBlock {
            ...NoonAccordianFields
        }
      }
    }
  }

  `);
  return data?.work;
}