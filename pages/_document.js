import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {

    render() {

        return (

            <Html lang="en">
                
                <Head>

                <link rel="stylesheet" href="https://use.typekit.net/cpm0ocs.css" />
                <script src="https://kit.fontawesome.com/9b8b7e93a7.js" crossOrigin="anonymous"></script>
                
                </Head>
                <body>
                <Main />
                <NextScript />
                </body>

                <link rel="stylesheet" src="/lib/scss/navigation/_navigationv3.scss" />

            </Html>

        )

    }

}