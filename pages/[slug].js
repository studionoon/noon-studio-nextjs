import Layout from '../components/layout'
import Components from '../components/utils/components'
import PostBody from '../components/post-body'
import { getAllPagesWithSlugs, getPageBySlug, getPrimaryMenu, getFooter1Menu, getFooter2Menu, getContactDetails } from '../lib/api';
import React from 'react';

function Page( { page, contactDetails, menus } ) {

    const blocks = page.blocks;

    return (

        <Layout page={page} menus={ menus } contact={contactDetails}  >
        
            <PostBody content={page.content} />
            
            { blocks.map( ( block, index ) => {
                return (
                    <React.Fragment key={index} >
                        {Components( block, index ) }
                    </React.Fragment>
                );
            } ) }


        </Layout>

    );

}

export async function getStaticProps({ params }) {
    const page = await getPageBySlug(params.slug);
    const menu_primary = await getPrimaryMenu();
    const menu_footer_1 = await getFooter1Menu();
    const menu_footer_2 = await getFooter2Menu();
    const contactDetails = await getContactDetails();
  
    if( params.slug == 'about'){

        // Add Noon Partner
        page.blocks.push( {
            "__typename": "NoonPartners",
            "__typename": "HomeApproach"
        }); 
        
    }

    return { 
        props: { 
            page, 
            menus: {
              menu_primary,
              menu_footer_1,
              menu_footer_2,
            }, 
            contactDetails,
        },
        revalidate: 10
    };

}

export async function getStaticPaths() {

    const pagesWithSlugs = await getAllPagesWithSlugs();

    return {
        paths: pagesWithSlugs.edges.map(({ node }) => `/${node.slug}`) || [],
        fallback: false
    };

}

export default Page;