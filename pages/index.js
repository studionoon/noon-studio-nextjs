import Layout from "../components/layout";
import Components from "../components/utils/components";
import WorkProjects from "../components/work/projects";
import Partners from "../components/work/partners";
import React, { Fragment } from "react";
import PostBody from "../components/post-body";
import HomeApproach from "../components/index/home-approach";
import {
  getPageBySlug,
  getAllPostsForHome,
  getPrimaryMenu,
  getFooter1Menu,
  getFooter2Menu,
  getContactDetails,
} from "../lib/api";

export default function Index({
  page,
  allPosts: { edges },
  preview,
  menus,
  contactDetails,
}) {
  const blocks = page.blocks;

  return (
    <>
      <Layout
        page={page}
        preview={preview}
        menus={menus}
        contact={contactDetails}
      >
        <PostBody content={page.content} />

        {blocks.map((block, index) => {
          return <Fragment key={index}>{Components(block, index)}</Fragment>;
        })}

        <WorkProjects toShow={3} showTitle={true} />
        <HomeApproach />
        <Partners />
      </Layout>
    </>
  );
}

export async function getStaticProps({ preview = false }) {
  const page = await getPageBySlug("/");
  const allPosts = await getAllPostsForHome(preview);
  const menu_primary = await getPrimaryMenu();
  const menu_footer_1 = await getFooter1Menu();
  const menu_footer_2 = await getFooter2Menu();
  const contactDetails = await getContactDetails();

  return {
    props: {
      page,
      allPosts,
      preview,
      menus: {
        menu_primary,
        menu_footer_1,
        menu_footer_2,
      },
      contactDetails,
    },
    revalidate: 10,
  };
}
