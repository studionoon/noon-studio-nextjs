import Layout from '../components/layout'
import { getPrimaryMenu, getFooter1Menu, getFooter2Menu, getContactDetails } from '../lib/api';
import styles from './404.module.scss'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useState, useEffect } from 'react';

const NoonButton = React.forwardRef(({ onClick, href }, ref) => {
    return (
        <a href={href} className={ `btn btn-accent btn-lg d-block` } tabIndex="-1" role="button" aria-disabled="true">Home</a>
    )
})

function page404( { contactDetails, menus } ) {

    const router = useRouter()

    return (

        <Layout menus={ menus } contact={contactDetails}  >
        
            <div className={ `${ styles['wrapper-404'] }` }>
                
                <div className={ `container ${ styles['container'] }` }>

                    <div className={ `row ${ styles['row'] }` }>

                        <div className={ `col ${styles['col-404']}` }>

                            <h1><span>404 error</span>Oops, something went wrong</h1>
                            <p className={ ` ${styles['intro']} `}>Try retyping the URL or return home</p>

                            <div className={ `row g-4`}>

                                <div className={ `col-12 col-md`}>

                                    <Link href="/" passHref legacyBehavior>

                                        <NoonButton />

                                     </Link>
                                    
                                </div>

                                <div className={ `col-12 col-md`}>

                                    <button type="button" onClick={() => router.back()} className={ `btn btn-white btn-lg d-block w-100` } tabIndex="-1" role="button" aria-disabled="true">
                                        Go Back
                                    </button>
                                    
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </Layout>

    );

}

export async function getStaticProps({ params }) {
    const menu_primary = await getPrimaryMenu();
    const menu_footer_1 = await getFooter1Menu();
    const menu_footer_2 = await getFooter2Menu();
    const contactDetails = await getContactDetails();
  
    return { 
        props: { 
            menus: {
              menu_primary,
              menu_footer_1,
              menu_footer_2,
            }, 
            contactDetails,
        },
        revalidate: 10
    };

}
export default page404;