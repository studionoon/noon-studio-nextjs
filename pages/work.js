import Layout from '../components/layout'
import Components from '../components/utils/components'
import PostBody from '../components/post-body'
import {  getPageBySlug, getPrimaryMenu, getFooter1Menu, getFooter2Menu, getContactDetails } from '../lib/api';
import React, { useState, useEffect } from 'react';

function Page( { page, contactDetails, menus } ) {

    const blocks = page.blocks;

    return (

        <Layout page={page} menus={ menus } contact={contactDetails}  >
        
            <PostBody content={page.content} />
            
            {/* {blocks.map(block => Components(block))} */}
            { blocks.map( ( block, index ) => {
                return (
                    <React.Fragment key={index} >
                        {Components( block, index ) }
                    </React.Fragment>
                );
            } ) }

	        {/* // #reactKeys */}

        </Layout>

    );

}

export async function getStaticProps({ params }) {
    const page = await getPageBySlug('our-work'); // ? coming through on /work
    const menu_primary = await getPrimaryMenu();
    const menu_footer_1 = await getFooter1Menu();
    const menu_footer_2 = await getFooter2Menu();
    const contactDetails = await getContactDetails();

    page.blocks.push( {
        "__typename": "NoonWorkHeader"

    }); 
    page.blocks.push( {
        "__typename": "NoonWorkProjects"          

    }); 
    page.blocks.push( {
        "__typename": "NoonPartners"
    }); 

    
    return { 
        props: { 
            page, 
            menus: {
              menu_primary,
              menu_footer_1,
              menu_footer_2,
            }, 
            contactDetails,
        },
        revalidate: 10
    };

}
export default Page;