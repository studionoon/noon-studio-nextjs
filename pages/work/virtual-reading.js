import Layout from "../../components/layout";
import WorkHeader from "../../components/work/work-header";
import {
  WorkOneColumn,
  WorkTwoColumn,
  WorkImage,
  WorkQuote,
} from "../../components/work/work-column";
import {
  getPrimaryMenu,
  getFooter1Menu,
  getFooter2Menu,
  getContactDetails,
} from "../../lib/api";

const projectDetails = {
  date: "June 2020",
  services: [
    "Design & Custom-build WordPress Development",
    "Digital Brand Identity",
    "API & Intergrations",
  ],
  title: "Virtual Experience Platform",
  client: "University of Reading",
  intro:
    "The University of Reading needed to bring their on-campus events online due to Covid-19, whilst still maintaining the excitement and interactivity.",
  quote: {
    message: [
      {
        class: "text-2xl font-bold max-w-none",
        tag: "p",
        content:
          "Studio Noon have provided us with a fantastic service, from both a technical and design perspective. They are solution driven, providing a range of options to help meet our needs…Our Virtual Open Day site was shortlisted for a Higher Education ‘Best Open Day’ award.",
      },
    ],
    name: "Rachel South",
    jobtitle: "Head of Global Recruitment, The University of Reading",
  },
};

// Images
import image_1 from "../../lib/images/veh-front-alt.jpg";
import image_2 from "../../lib/images/whiteboard-project.jpg";

function Page({ contactDetails, menus }) {
  return (
    <Layout menus={menus} contact={contactDetails}>
      <WorkHeader {...projectDetails} />
      <WorkImage
        image={image_1}
        classes="h-[300px] lg:h-[750px]"
        objectPosition="50% 90%"
      />
      <WorkOneColumn
        content={[
          {
            class: "max-w-none text-primary-65",
            tag: "p",
            content:
              "Home to more than 23,000 students, The University of Reading is a global university with a world-class reputation for teaching, research and enterprise.",
          },
          {
            class: "max-w-none text-primary-65",
            tag: "p",
            content:
              "Following the outbreak of the Covid-19 pandemic, the University of Reading were looking to bring their Open Days and on-campus events online in a new virtual events format. They wanted to retain the excitement and interactivity that on-campus events delivered.",
          },
        ]}
        title="context"
      />
      <WorkImage
        image={image_2}
        isFull={false}
        classes="h-[300px] lg:h-[500px]"
        objectPosition="50% 15%"
      />
      <WorkTwoColumn
        content={[
          [
            {
              class: "intro text-primary-65",
              tag: "p",
              content:
                "We started by exploring user journeys from students at on-campus events, looking at how they digested information and how we could replicate this in a virtual format. What we found was that visitors prioritised key aspects of an open day, so as part of the platform we looked to develop an onboarding process which guided visitors to get close to the same experience as at the physical events.",
            },
          ],
          [
            {
              class: "intro text-primary-65",
              tag: "p",
              content:
                "The platform also allowed integration of live events, which housed live interactive sessions, and subjects pages displayed in a user-friendly way.",
            },
            {
              class: "intro text-primary-65",
              tag: "p",
              content:
                "We provided guidelines which has allowed Reading to implement this visual identity across more of their digital assets.",
            },
          ],
        ]}
        title="Our solution"
        header="Developing an immersive platform"
      />
      <WorkImage image={image_1} isFull={false} />
      <WorkOneColumn
        content={[
          {
            class: "max-w-none text-primary-65",
            tag: "p",
            content:
              "Following the first Virtual Open Day, a survey found that 98% of prospective students rated their experience as good or excellent.",
          },
          {
            class: "max-w-none text-primary-65",
            tag: "p",
            content:
              "The Virtual Open Day hosted on the platform was shortlisted for the Higher Education Awards ‘Best Open Day Experience’ in June 2020.",
          },
          {
            class: "max-w-none text-primary-65",
            tag: "p",
            content:
              "The platform has proven its success long after lockdowns, we’ve continued working with the University’s Global Recruitment team to develop the platform further, continuing to evolve as requirements change.",
          },
        ]}
        title="The Results"
      />
      <WorkQuote {...projectDetails} />
    </Layout>
  );
}

export async function getStaticProps({ params }) {
  const menu_primary = await getPrimaryMenu();
  const menu_footer_1 = await getFooter1Menu();
  const menu_footer_2 = await getFooter2Menu();
  const contactDetails = await getContactDetails();

  return {
    props: {
      menus: {
        menu_primary,
        menu_footer_1,
        menu_footer_2,
      },
      contactDetails,
    },

    revalidate: 10,
  };
}
export default Page;
