import Layout from '../../components/layout'
import PostBody from '../../components/post-body'
import { getPrimaryMenu, getFooter1Menu, getFooter2Menu, getContactDetails } from '../../lib/api';
import WorkHeader from '../../components/work/hospitality/header';
import WorkMediaText from '../../components/work/hospitality/mediaText';
import WorkTextCol from '../../components/work/hospitality/textCol';
import WorkProjectGallery from '../../components/work/hospitality/projectGallery';
import WorkStats from '../../components/work/hospitality/stats';
import WorkQuote from '../../components/work/hospitality/quote';
import RelatedProject from '../../components/work/hospitality/related-project';



function Page( { contactDetails, menus } ) {

    return (

        <Layout  menus={ menus } contact={contactDetails}  >

            <WorkHeader />
            <WorkMediaText />

            {/* workCols */}
            <WorkTextCol />

            {/* workGallery */}
            <WorkProjectGallery />

            {/* workStats */}
            <WorkStats />

            {/* workQuote */}
            <WorkQuote />

            <RelatedProject />

        </Layout>

    );

}

export async function getStaticProps({ params }) {
    const menu_primary = await getPrimaryMenu();
    const menu_footer_1 = await getFooter1Menu();
    const menu_footer_2 = await getFooter2Menu();
    const contactDetails = await getContactDetails();
  
    return { 

        props: { 
            menus: {
              menu_primary,
              menu_footer_1,
              menu_footer_2,
            }, 
            contactDetails,
        },

        revalidate: 10,

    };

}
export default Page;