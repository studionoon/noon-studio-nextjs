import Layout from '../../components/layout'
import PostBody from '../../components/post-body'
import { getPrimaryMenu, getFooter1Menu, getFooter2Menu, getContactDetails } from '../../lib/api';
import WorkHeader from '../../components/work/reading-museum/header';
import WorkQuote from '../../components/work/reading-museum/quote';
import WorkAccordionOrange from '../../components/work/reading-museum/accordionorange';
import WorkAccordionWhite from '../../components/work/reading-museum/accordionwhite';
import WorkAccordionRed from '../../components/work/reading-museum/accordionred';
import RelatedProject from '../../components/work/reading-museum/related-project';



function Page( { contactDetails, menus } ) {

    return (

        <Layout  menus={ menus } contact={contactDetails}  >

            <WorkHeader />

            <WorkAccordionOrange />

            <WorkAccordionWhite />
            
            <WorkAccordionRed />

            <WorkQuote />

            <RelatedProject />

        </Layout>

    );

}

export async function getStaticProps({ params }) {
    const menu_primary = await getPrimaryMenu();
    const menu_footer_1 = await getFooter1Menu();
    const menu_footer_2 = await getFooter2Menu();
    const contactDetails = await getContactDetails();
  
    return { 

        props: { 
            menus: {
              menu_primary,
              menu_footer_1,
              menu_footer_2,
            }, 
            contactDetails,
        },

        revalidate: 10,

    };

}
export default Page;