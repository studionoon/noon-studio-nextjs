import Layout from '../../components/layout'
import PostBody from '../../components/post-body'
import { getPrimaryMenu, getFooter1Menu, getFooter2Menu, getContactDetails } from '../../lib/api';
import WorkHeader from '../../components/work/growing-for-a-digital-age/header';
import WorkMediaText from '../../components/work/growing-for-a-digital-age/mediaText';
import WorkTextCol from '../../components/work/growing-for-a-digital-age/textCol';
import WorkProjectGallery from '../../components/work/growing-for-a-digital-age/projectGallery';
import WorkStats from '../../components/work/growing-for-a-digital-age/stats';
import WorkQuote from '../../components/work/growing-for-a-digital-age/quote';
import ImageLeadEmails from '../../components/work/growing-for-a-digital-age/image-lead-emails';
import RelatedProject from '../../components/work/growing-for-a-digital-age/related-project';

function Page( { contactDetails, menus } ) {

    return (

        <Layout  menus={ menus } contact={contactDetails}  >

            <WorkHeader />
            <WorkMediaText />

            {/* workCols */}
            <WorkTextCol />

            {/* workGallery */}
            <WorkProjectGallery />


            {/* <ImageLeadEmails /> */}
            
            {/* workStats */}
            <WorkStats />

            {/* workQuote */}
            <WorkQuote />

            {/* Related Project */}
            <RelatedProject />

        </Layout>

    );

}

export async function getStaticProps({ params }) {
    const menu_primary = await getPrimaryMenu();
    const menu_footer_1 = await getFooter1Menu();
    const menu_footer_2 = await getFooter2Menu();
    const contactDetails = await getContactDetails();
  
    return { 

        props: { 
            menus: {
              menu_primary,
              menu_footer_1,
              menu_footer_2,
            }, 
            contactDetails,
        },

        revalidate: 10,

    };

}
export default Page;