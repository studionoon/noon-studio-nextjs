import Layout from '../../components/layout'
import PostBody from '../../components/post-body'
import { getPrimaryMenu, getFooter1Menu, getFooter2Menu, getContactDetails } from '../../lib/api';
import WorkHeader from '../../components/work/university-of-surrey/header.js';
import WorkMediaText from '../../components/work/university-of-surrey/mediaText.js';
import WorkTextCol from '../../components/work/university-of-surrey/textCol.js';
import WorkProjectGallery from '../../components/work/university-of-surrey/projectGallery.js';
import WorkStats from '../../components/work/university-of-surrey/stats.js';
import WorkQuote from '../../components/work/university-of-surrey/quote.js';
import RelatedProject from '../../components/work/university-of-surrey/related-project.js';



function Page( { contactDetails, menus } ) {

    return (

        <Layout  menus={ menus } contact={contactDetails}  >

            <WorkHeader />
            <WorkMediaText />

            {/* workCols */}
            <WorkTextCol />

            {/* workGallery */}
            <WorkProjectGallery />

            {/* workStats */}
            <WorkStats />

            {/* workQuote */}
            <WorkQuote />

            <RelatedProject />

        </Layout>

    );

}

export async function getStaticProps({ params }) {
    const menu_primary = await getPrimaryMenu();
    const menu_footer_1 = await getFooter1Menu();
    const menu_footer_2 = await getFooter2Menu();
    const contactDetails = await getContactDetails();
  
    return { 

        props: { 
            menus: {
              menu_primary,
              menu_footer_1,
              menu_footer_2,
            }, 
            contactDetails,
        },

        revalidate: 10,

    };

}
export default Page;