import Layout from '../../components/layout'
import { getPrimaryMenu, getFooter1Menu, getFooter2Menu, getContactDetails } from '../../lib/api';
import WorkHeader from '../../components/work/imagine/header';
import Mockup from '../../components/work/imagine/mockup';
import Impact from '../../components/work/imagine/impact';



function Page( { contactDetails, menus } ) {

    return (

        <Layout  menus={ menus } contact={contactDetails}  >


            <WorkHeader />

            <Mockup />

            <Impact />

        </Layout>

    );

}

export async function getStaticProps({ params }) {
    const menu_primary = await getPrimaryMenu();
    const menu_footer_1 = await getFooter1Menu();
    const menu_footer_2 = await getFooter2Menu();
    const contactDetails = await getContactDetails();
  
    return { 

        props: { 
            menus: {
              menu_primary,
              menu_footer_1,
              menu_footer_2,
            }, 
            contactDetails,
        },

        revalidate: 10,

    };

}
export default Page;