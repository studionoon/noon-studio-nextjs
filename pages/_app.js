import "../styles/index.css";
import "../lib/scss/_variables.scss";
import "../lib/scss/bootstrap.scss";
import "../lib/scss/core.scss";
import "../lib/scss/global.scss";

import { GoogleAnalytics } from "nextjs-google-analytics";
import { useEffect } from "react";

// The following import prevents a Font Awesome icon server-side rendering bug,
// where the icons flash from a very large icon down to a properly sized one:
import "@fortawesome/fontawesome-svg-core/styles.css";
// Prevent fontawesome from adding its CSS since we did it manually above:
import { config } from "@fortawesome/fontawesome-svg-core";
config.autoAddCss = false; /* eslint-disable import/first */

// export function reportWebVitals(metric) {
//     console.log(metric)
// }

function NoonSite({ Component, pageProps }) {
  useEffect(() => {
    import("bootstrap/dist/js/bootstrap");

    document.body.addEventListener("mousedown", function () {
      document.body.classList.add("using-mouse");
    });

    // Re-enable focus styling when Tab is pressed
    document.body.addEventListener("keydown", function (event) {
      if (event.keyCode === 9) {
        document.body.classList.remove("using-mouse");
      }
    });
  }, []);

  return (
    <>
      <GoogleAnalytics trackPageViews />
      <Component {...pageProps} />
    </>
  );
}

export default NoonSite;
